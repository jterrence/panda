var krpanoplugin = function() {
    function fa(a) {
        var c = {
            type: a.type || "GET",
            url: a.url || "",
            async: a.async || "true",
            data: a.data || null,
            dataType: a.dataType || "text",
            contentType: a.contentType || "application/x-www-form-urlencoded",
            beforeSend: a.beforeSend ||
            function() {},
            success: a.success ||
            function() {},
            error: a.error ||
            function() {}
        };
        c.beforeSend();
        var b;
        b = window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : window.XMLHttpRequest ? new XMLHttpRequest: void 0;
        b.responseType = c.dataType;
        b.open(c.type, c.url, c.async);
        b.setRequestHeader("Content-Type", c.contentType);
        b.send(ha(c.data));
        b.onreadystatechange = function() {
            4 == b.readyState && (200 == b.status ? c.success(b.response) : c.error())
        }
    }
    function ha(a) {
        if ("object" === typeof a) {
            var c = "",
                b;
            for (b in a) c += b + "=" + a[b] + "&";
            return c = c.substring(0, c.length - 1)
        }
        return a
    }
    function M(a) {
        return "boolean" == typeof a ? a: 0 <= "yesontrue1".indexOf(String(a).toLowerCase())
    }
    function d(a, b, d, e, g) {
        3 == a || 4 == a || 5 == a ? c[b] = d: c.registerattribute(b, d, e, g);
        E.push(b)
    }
    function m(a, c, b, e) {
        a.addEventListener(c, b, e);
        F.push({
            obj: a,
            eventname: c,
            callback: b,
            capture: e
        })
    }
    function x(a) {
        var c, b = F.length,
            e;
        for (c = 0; c < b; c++) if (e = F[c], null == a || e.obj === a) e.obj.removeEventListener(e.eventname, e.callback, e.capture),
            F.splice(c, 1),
            c--,
            b--
    }
    function ia() {
        function a(a) {
            p = !0;
            0 < n && (e.seek(n), n = -1)
        }
        function d(a) {
            g && f && (N(f.src + " - loading failed"), f = null)
        }
        function ga() {
            if (g && b && !(2 > b.readyState)) {
                var a = h.timertick,
                    e = 0,
                    d = Number(b.duration);
                isNaN(d) || 0 >= d || (f ? (k && !f.paused ? f.pause() : !k && f.paused && f.play(), e = f.currentTime) : k ? e = r: (0 == t && (t = a), e = r + A * Math.max(0, (a - t) / 1E3)), e >= d - .02 ? (e = d, y ? (r = 0, t = a + .1, f && (f.currentTime = 0)) : (z(), 0 == c.iscomplete && (c.iscomplete = !0, h.call(c.onvideocomplete, c)))) : c.iscomplete = !1, u = e, .01 < Math.abs(b.currentTime - u) && (b.currentTime = Number(u.toFixed(2))), b.autoplay = !0)
            }
        }
        var e = this,
            k = e.paused = !0,
            l = null,
            f = null,
            B = null,
            p = !1,
            n = -1,
            t = 0,
            r = 0; (function() {
            if (!0 !== window.krpanoHideiPhoneMediaControlsStyle) {
                window.krpanoHideiPhoneMediaControlsStyle = !0;
                var a = document.createElement("style");
                a.type = "text/css";
                a.innerHTML = "video::-webkit-media-controls{display:none!important;}";
                document.getElementsByTagName("head")[0].appendChild(a)
            }
        })();
        e.start = function() {
            c.iPhoneMode = !0;
            b.autoplay = !0;
            b.pause();
            b.style.webkitMediaControls = !1;
            var g = S(c.videourl, ["m4a", "mp3"]),
                g = h.parsePath(g);
            n = -1;
            g ? (null == f && (B ? (f = B, B = null) : f = document.createElement("audio")), x(f), m(f, "canplay", a, !0), m(f, "error", d, !0), f.loop = y, p = f.autoplay = !1, f.src = g, f.load(), f.pause()) : f && (f.src && (f.pause(), B = f), f = null);
            t = r = 0;
            k = c.pausedonstart;
            e.paused = k;
            l = setInterval(ga, 1E3 / 60);
            b.currentTime = 0
        };
        e.play_audio = function() {
            f && f.play()
        };
        e.play = function() {
            1 == k && (k = e.paused = !1, c.iscomplete ? (c.iscomplete = !1, r = 0, f && (f.currentTime = 0)) : r = b.currentTime, t = h.timertick + .1, f && f.play())
        };
        e.pause = function() {
            0 == k && (f && f.pause(), r = b.currentTime, k = e.paused = !0)
        };
        e.seek = function(a) {
            f ? p ? (f.currentTime = a, n = -1) : n = a: (n = -1, t = 0, r = a)
        };
        e.remove = function() {
            l && (clearInterval(l), l = null);
            f && (f.src && (f.pause(), B = f), f = null)
        };
        e.setplaybackrate = function(a) {
            if (f) try {
                f.playbackRate = a
            } catch(c) {}
        };
        e.need_touchstart_play = function() {
            return null != f && f.paused
        };
        e.try_touchstart_play = function() {
            return f ? (f.play(), 0 == f.paused) : !0
        }
    }
    function N(a) {
        var b = c ? c.onerror: null;
        null != b && "" != b && "null" != ("" + b).toLowerCase() ? (c.videoerror = a, h.call(b, c)) : h.trace(3, a + "!")
    }
    function T(a) {
        a.setAttribute("x5-video-" + i + "-type", "h5");
        a = a.style;
        a.pointerEvents = "none";
        a.position = "absolute";
        a.width = "100%";
        a.height = "100%";
        a.left = 0;
        a.top = 0;
        a.opacity = 1;
        a.backgroundColor = "transparent";
        a[C] = "translateZ(0)";
        a[C + "Origin"] = "0 0"
    }
    function U() {
        var a = null,
            a = document.createElement("video");
        if (!a || !a.play) return null;
        T(a);
        for (var c = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split(""), b = "", e = 0; 8 > e; e++) b += c[Math.ceil(35 * Math.random())];
        return a
    }
    function O(a) {
        c && (a = document.visibilityState, !0 === document.hidden || "hidden" == a || "prerender" == a || "unloaded" == a ? 0 == c.ispaused && c.autopause && 0 == c.isautopaused && (c.isautopaused = !0, z()) : c.autoresume && c.isautopaused && (c.isautopaused = !1, G()))
    }
    function V(a, b) {
        c.registercontentsize(a, b);
        c.forceresize = !0;
        c.videowidth = a;
        c.videoheight = b;
        c.havevideosize = !0;
        c.isvideoready = !0;
        if (c.onvideoreadyCB) c.onvideoreadyCB();
        h.call(c.onvideoready, c)
    }
    function S(a, b) {
        v = null;
        var d = ("" + a).split("|");
        if (1 < d.length || b) {
            var e = p;
            b && (e = b);
            var g = e.length,
                k = d.length,
                f, h, m = [];
            for (f = 0; f < k; f++) if (h = d[f]) {
                var n = h;
                if (0 != h.indexOf("rtmp:")) {
                    var l = h.indexOf("?");
                    0 < l && (h = h.slice(0, l));
                    l = h.indexOf("#");
                    0 < l && (h = h.slice(0, l));
                    l = h.lastIndexOf(".");
                    if (1 < l) for (l = ("" + h.slice(l + 1)).toLowerCase(), h = 0; h < g; h++) if (l == e[h]) {
                        if (b) return n;
                        m.push({
                            type: l,
                            inorder: f,
                            url: n,
                            used: !1
                        });
                        break
                    }
                }
            }
            if (b) return null;
            if (0 < m.length) return "" != c.preferredformat && m.sort(function(a, b) {
                var d = a.type,
                    e = b.type,
                    f = "m3u" == d || "m3u8" == d ? 0 : "mp4" == d || "m4v" == d ? 1 : "webm" == d ? 2 : 3,
                    h = "m3u" == e || "m3u8" == e ? 0 : "mp4" == e || "m4v" == e ? 1 : "webm" == e ? 2 : 3,
                    g = ("" + c.preferredformat).toLowerCase();
                "" != g && (d == g && (f = -1), e == g && (h = -1));
                return f > h ? 1 : f < h ? -1 : a.inorder > b.inorder ? 1 : a.inorder < b.inorder ? -1 : 0
            }),
                v = m,
                W()
        }
        return a
    }
    function W() {
        if (v) {
            var a, b;
            b = v.length;
            for (a = 0; a < b; a++) if (0 == v[a].used) return v[a].used = !0,
                v[a].url
        }
        return null
    }
    function P(a, b) {
        var c = !0;
        if (!n.android || !n.chrome) {
            var d = b.indexOf("://");
            if (0 < d) {
                var g = document.domain,
                    d = b.slice(d + 3, b.indexOf("/", d + 3));
                g == d && (c = !1)
            }
        }
        c && ((c = h.security.cors) && "" != c || (c = "anonymous"), a.crossOrigin = c)
    }
    function X(a, d, l, e) {
        c.videourl = void 0 === a || null == a || "" == a || "null" == ("" + a).toLowerCase() ? null: a;
        c.posterurl = void 0 === d || null == d || "" == d || "null" == ("" + d).toLowerCase() ? null: d;
        c.pausedonstart = M(l);
        e = Number(e);
        if (isNaN(e) || 0 > e) e = 0;
        u = e;
        D = 0 < e ? e: -1;
        a = S(c.videourl);
        w = a = h.parsePath(a);
        c.isvideoready = !1;
        c.havevideosize = !1;
        c.isautopaused = !1;
        c.isvideoready = !1;
        c.isseeking = 0 < D && null == g;
        c.iscomplete = !1;
        c.ispaused = !0;
        c.loadedbytes = 0;
        c.totalbytes = 0;
        c.totaltime = 0;
        c.videoerror = "";
        null != a && (b && b.src ? g ? g.pause() : b.pause() : null == b && (b = U(), c.videoDOM = b), c.posterurl && (c.pausedonstart || n.mobile || n.tablet) ? (null == k && (k = document.createElement("img"), m(k, "error", ka, !1), m(k, "load", la, !1)), a = c.posterurl, Q = a = h.parsePath(a), P(k, a), k.src = a) : Y())
    }
    function ka(a) {
        b && N(Q + " - loading failed")
    }
    function la(a) {
        if (b) {
            a = k.naturalWidth;
            var d = k.naturalHeight;
            T(k);
            c.sprite && (c.sprite.appendChild(k), Array.prototype.indexOf.call(k.parentNode.children, k));
            c.posterDOM = k;
            V(a, d);
            Y()
        }
    }
    function Y() {
        x(b);
        q && (clearInterval(q), q = null);
        g && g.remove();
        m(b, "loadedmetadata", l, !1);
        m(b, "loadeddata", l, !1);
        m(b, "canplay", l, !1);
        m(b, "canplaythrough", l, !1);
        m(b, "play", l, !1);
        m(b, "pause", l, !1);
        m(b, "playing", l, !1);
        m(b, "seeking", l, !1);
        m(b, "waiting", l, !1);
        m(b, "seeked", l, !1);
        m(b, "stalled", l, !1);
        m(b, "suspend", l, !1);
        m(b, "ended", l, !1);
        m(b, "timeupdate", l, !1);
        m(b, "progress", H, !1);
        n.ios && (q = setInterval(H, 500));
        m(b, "error", ma, !1);
        b.loop = c.loop;
        b.autoplay = c.pausedonstart ? !1 : !0;
        b.preload = c.html5preload;
        b.controls = c.html5controls;
        b.playsinline = b["webkit-playsinline"] = !0;
        Z(I);
        aa(A);
        ba(J);
        P(b, w);
        b.src = w;
        b.load();
        g ? g.start() : c.pausedonstart ? b.pause() : b.play()
    }
    function ma(a) {
        if (b) {
            a = b.error ? b.error.code: 0;
            if (3 <= a) {
                var c = W();
                if (c) {
                    w = h.parsePath(c);
                    P(b, w);
                    b.src = w;
                    b.load();
                    return
                }
            }
            switch (a) {
                case 1:
                    a = "video loading aborted";
                    break;
                case 2:
                    a = "network loading error";
                    break;
                case 3:
                    a = "video decoding failed (corrupted data or unsupported codec)";
                    break;
                case 4:
                    a = "loading video failed";
                    break;
                default:
                    a = "unknown error"
            }
            a && N(w + " - " + a)
        }
    }
    function H(a) {
        null != q && a && "progress" == a.type && (clearInterval(q), q = null);
        if (b && b.buffered) {
            var d, h;
            d = b.buffered.length;
            h = b.duration;
            if (0 < h && (c.totaltime = h, 0 < d)) {
                var e = 0;
                for (a = 0; a < d; a++) {
                    var g = b.buffered.end(a);
                    g > e && (e = g)
                }
                c.loadedbytes = 1048576 * e | 0;
                c.totalbytes = 1048576 * h | 0
            }
        }
    }
    function l(a) {
        if (b) switch (a.type) {
            case "loadedmetadata":
            case "loadeddata":
                0 < D && (K(D), D = -1);
                H();
                a = b.videoWidth;
                var d = b.videoHeight;
                0 == c.havevideosize && 0 < a && 0 < d && V(a, d);
                break;
            case "canplay":
            case "canplaythrough":
                H();
                null == g ? 0 == c.pausedonstart && b.paused && (b.play(), R()) : 0 == c.pausedonstart && g.need_touchstart_play() && (g.play_audio(), R());
                break;
            case "seeking":
            case "seeked":
                c.isseeking = g ? !1 : "seeking" == a.type;
            case "play":
            case "pause":
            case "playing":
            case "waiting":
            case "stalled":
            case "suspend":
            case "ended":
            case "timeupdate":
                a = !1,
                k && 2 <= b.readyState && (0 == b.paused || g && 0 == g.paused) && (a = !0, n.chromemobile && 0 == b.currentTime && (a = !1)),
                a && (x(k), k.parentNode && k.parentNode.removeChild(k), k = Q = c.posterDOM = null),
                c.isvideoready && (a = g ? g: b, c.ispaused != a.paused && (0 == a.paused ? (c.ispaused = !1, h.call(c.onvideoplay, c)) : (c.ispaused = !0, h.call(c.onvideopaused, c))), null == g && c.iscomplete != b.ended && (1 == b.ended ? (z(), c.iscomplete = !0, h.call(c.onvideocomplete, c)) : c.iscomplete = !1))
        }
    }
    function R() {
        n.touch && (b.paused && !g || g && g.need_touchstart_play()) && 0 == L && (L = !0, c.touchworkarounds && (m(document.body, "touchstart", ca, !0), m(document.body, "touchend", ca, !0)), c.needtouch = !0, h.call(c.onneedtouch, c))
    }
    function da() {
        L && (L = !1, x(document.body), c.needtouch = !1, h.call(c.ongottouch, c))
    }
    function ca(a) {
        b && (g ? a = g.try_touchstart_play() : (b.play(), a = 0 == b.paused), a && da())
    }
    function na(a) {
        y = M(a);
        b && (b.loop = y)
    }
    function oa() {
        return y
    }
    function pa(a) {
        K(a)
    }
    function qa() {
        if (b) {
            var a = Number(b.currentTime);
            if (!isNaN(a)) return a
        }
        return u
    }
    function Z(a) {
        a = Number(a);
        isNaN(a) ? a = 1 : 0 > a ? a = 0 : 1 < a && (a = 1);
        I = a;
        b && (b.volume = a)
    }
    function ra() {
        return I
    }
    function aa(a) {
        a = Number(a);
        if (isNaN(a) || 0 == a) a = 1;
        A = a;
        if (b) try {
            b.playbackRate = a
        } catch(c) {}
        g && g.setplaybackrate(a)
    }
    function sa() {
        return A
    }
    function ba(a) {
        J = a = M(a);
        b && n.safari && (b.airplay = b["x-webkit-airplay"] = a ? "allow": "disallow")
    }
    function ta() {
        return J
    }
    function G() {
        g ? g.play() : b && b.play();
        c.pausedonstart = !1;
        R()
    }
    function z() {
        g ? g.pause() : b && b.pause();
        c.pausedonstart = !0;
        da()
    }
    function ua() {
        b && (0 == b.paused || g && 0 == g.paused ? z() : G())
    }
    function K(a) {
        if (b && b.src) {
            var c = 0,
                c = 0 < ("" + a).indexOf("%") && 0 < b.duration ? parseFloat(a) / 100 * b.duration: Number(a);
            isNaN(c) || (g ? g.seek(c) : b.currentTime = c)
        }
    }
    function va() {
        K(0);
        z()
    }
    function ea() {
        b && (b.pause(), g && g.remove(), x(b), k && k.parentNode && k.parentNode.removeChild(k), b && b.parentNode && b.parentNode.removeChild(b), c.videoDOM = null, c.posterDOM = null, c.iPhoneMode = !1, b = k = null, c.videourl = null, c.isvideoready = !1, c.ispaused = !0, c.iscomplete = !1, c.isseeking = !1, c.isautopaused = !1, c.havevideosize = !1, c.videowidth = 0, c.videoheight = 0, c.loadedbytes = 0, c.totalbytes = 0, c.totaltime = 0, c.videoerror = "", u = 0)
    }
    var i = "player";
    this.name = "Videoplayer";
    this.version = "1.19-pr6";
    this.build = "2016-08-05";
    var h = null,
        n = null,
        c = null,
        b = null,
        k = null,
        Q = null,
        w = null,
        v = null,
        p = [],
        C = "transform",
        g = null,
        D = -1,
        L = !1,
        q = null,
        y = !1,
        I = 1,
        A = 1,
        J = !1,
        u = 0,
        E = [],
        F = [];
    this.registerplugin = function(a, l, q) {
        h = a;
        n = h.device;
        c = q;
        "1.19" > h.version || "2015-03-01" > h.build ? h.trace(3, "Videoplayer plugin - too old krpano version (min. 1.19)") : (C = h.browser.css.transform, (b = U()) ? (void 0 !== b.canPlayType ? (b.canPlayType("video/mp4").match(/maybe|probably/i) && (p.push("mp4"), p.push("m4v"), p.push("mov"), p.push("3gp")), b.canPlayType("video/webm").match(/maybe|probably/i) && p.push("webm"), b.canPlayType("video/ogg").match(/maybe|probably/i) && (p.push("ogg"), p.push("ogv")), b.canPlayType("video/hls").match(/maybe|probably/i) && (p.push("m3u"), p.push("m3u8"))) : (p.push("mp4"), p.push("mov")), d(0, "videourl", null), d(0, "altvideourl", null), d(0, "posterurl", null), d(0, "panovideo", !1), d(0, "use_as_videopano", !1), d(0, "pausedonstart", !1), d(0, "autopause", !0), d(0, "autoresume", !1), d(0, "preferredformat", ""), d(0, "iphoneworkarounds", !0), d(0, "touchworkarounds", !0), d(0, "html5controls", !1), d(0, "html5preload", "auto"), d(1, "loop", y, na, oa), d(1, "time", u, pa, qa), d(1, "volume", I, Z, ra), d(1, "playbackrate", A, aa, sa), d(1, "airplay", J, ba, ta), d(2, "onvideoready", null), d(2, "onvideoplay", null), d(2, "onvideopaused", null), d(2, "onvideocomplete", null), d(2, "onerror", null), d(2, "onneedtouch", null), d(2, "ongottouch", null), d(3, "playvideo", X), d(3, "play", G), d(3, "resume", G), d(3, "pause", z), d(3, "togglepause", ua), d(3, "seek", K), d(3, "stop", va), d(3, "closevideo", ea), d(4, "isvideoready", !1), d(4, "iswaiting", !1), d(4, "ispaused", !0), d(4, "iscomplete", !1), d(4, "isseeking", !1), d(4, "isautopaused", !1), d(4, "havevideosize", !1), d(4, "needtouch", !1), d(4, "videowidth", 0), d(4, "videoheight", 0), d(4, "loadedbytes", 0), d(4, "totalbytes", 0), d(4, "totaltime", 0), d(4, "videoerror", ""), d(5, "videoDOM", b), d(5, "posterDOM", k), d(5, "iPhoneMode", !1), m(window, "pagehide", O, !1), m(window, "pageshow", O, !1), m(document, "visibilitychange", O, !1), n.iphone && 1 == c.iphoneworkarounds && (g = new ia), c.sprite && (0 == c.use_as_videopano && 0 == c.panovideo || !n.panovideosupport) && !0 !== c.renderToBitmap && (c.sprite.appendChild(b), Array.prototype.indexOf.call(b.parentNode.children, b)), X(c.altvideourl ? c.altvideourl: c.videourl, c.posterurl, c.pausedonstart, c.time)) : h.trace(2, "Videoplayer plugin - HTML5 video is not supported by this browser!"))
    };
    this.unloadplugin = function() {
        ea();
        var a, b = E.length;
        for (a = 0; a < b; a++) delete c[E[a]];
        E = null;
        x(null);
        q && (clearInterval(q), q = null);
        h = n = c = null
    };
    this.onresize = function(a, d) {
        if (b && c && !0 !== c.renderToBitmap) {
            var g = b.videoWidth,
                e = b.videoHeight;
            if (0 < g && 0 < e) {
                var l = g + "px",
                    m = e + "px",
                    f = "hotspot" == c._type && c.distorted ? 1 : h.stagescale,
                    g = "translateZ(0) scale(" + a * f / g + "," + d * f / e + ")";
                k && (l != k.style.width && (k.style.width = l), m != k.style.width && (k.style.height = m), k.style[C] = g);
                l != b.style.width && (b.style.width = l);
                m != b.style.height && (b.style.height = m);
                b.style[C] = g
            }
        }
        return ! 1
    }
};

krpanoplugin();