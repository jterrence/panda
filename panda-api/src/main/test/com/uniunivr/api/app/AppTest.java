package com.uniunivr.api.app;

import com.uniunivr.api.dao.CustomerMapper;
import com.uniunivr.api.dao.OrderConfigMapper;
import com.uniunivr.api.dao.SerialNumberMapper;
import com.uniunivr.api.dao.ShareMapper;
import com.uniunivr.api.pojo.*;
import com.uniunivr.api.service.AppService;
import com.uniunivr.api.service.ProductToolsService;
import com.uniunivr.api.service.ShareService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.Date;
import java.util.List;


public class AppTest{

    private AppService mAppSerivce;


    @Before
    public void before(){
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.getEnvironment().setActiveProfiles("dev");
        ctx.load(new String[]{"classpath:spring-mybatis.xml" ,
                "classpath:spring.xml", "classpath:spring-mvc.xml"});
        ctx.refresh();
//        ctx.setEnvironment();
        mAppSerivce = (AppService) ctx.getAutowireCapableBeanFactory().getBean("appServiceImpl");
    }

    @Test
    public void testInsert(){
//        final AppVersion appVersion = new AppVersion();
//        appVersion.setAppId("com.uni.pano");
//        appVersion.setVersionName("UU360_v1.1.2_20170605_B");
//        appVersion.setVersionCode(1);
//        appVersion.setChannelId(1);
//        appVersion.setChannelName("官网");
//        appVersion.setAppType("android");
//        appVersion.setDownloadUrl("www.uniunivr.com");
//        final int row = mAppSerivce.insert(appVersion);
//        System.out.println("row---->" + row);
    }

}
