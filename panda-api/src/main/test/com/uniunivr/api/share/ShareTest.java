package com.uniunivr.api.share;

import com.uniunivr.api.dao.CustomerMapper;
import com.uniunivr.api.dao.OrderConfigMapper;
import com.uniunivr.api.dao.SerialNumberMapper;
import com.uniunivr.api.dao.ShareMapper;
import com.uniunivr.api.pojo.Customer;
import com.uniunivr.api.pojo.OrderConfig;
import com.uniunivr.api.pojo.SerialNumber;
import com.uniunivr.api.pojo.Share;
import com.uniunivr.api.service.ProductToolsService;
import com.uniunivr.api.service.ShareService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.Date;
import java.util.List;


public class ShareTest{

    private ShareService userService;

//    private UserDaoImpl mUserDao;

    private ShareMapper mShareMapper;

    private CustomerMapper mCustomerMapper;

    private SerialNumberMapper mSerialNumberMapper;

    private OrderConfigMapper mOrderConfigMapper;

    private ProductToolsService mProductToolsService;

    @Before
    public void before(){
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.getEnvironment().setActiveProfiles("dev");
        ctx.load(new String[]{"classpath:spring-mybatis.xml" ,
                "classpath:spring.xml", "classpath:spring-mvc.xml"});
        ctx.refresh();
//        userService = (ShareService) context.getBean("shareServiceImpl");
//        mShareMapper = (ShareMapper) ctx.getAutowireCapableBeanFactory().getBean("shareDao");

        mCustomerMapper = (CustomerMapper) ctx.getAutowireCapableBeanFactory().getBean("customerMapper");

        mSerialNumberMapper = (SerialNumberMapper)ctx.getAutowireCapableBeanFactory().getBean("serialNumberMapper");

        mOrderConfigMapper = (OrderConfigMapper)ctx.getAutowireCapableBeanFactory().getBean("orderConfigMapper");

        mProductToolsService = (ProductToolsService)ctx.getAutowireCapableBeanFactory().getBean("productServiceToolsImpl");
    }

    @Test
    public void testSelectShareId(){
    }

    @Test
    public void testGetUploadFileToken(){
//        final UploadFileModel uploadFileModel = userService.getUploadFileResponse();
//        System.out.println("返回状态码为--》" + uploadFileModel.getRtn());
    }

    @Test
    public void testShareInfo(){

    }

    @Test
    public void addCustomer(){
        for(int i = 0; i< 5;i ++ ){
            final Customer customer = new Customer();
            customer.setCustomerName("联坤科技 -- > " + i);
            customer.setCustomerId((long) 1111111111 + i);
            customer.setCreateTime(new Date());
            customer.setIsDelete((byte) 0);
            mCustomerMapper.insertSelective(customer);
        }
    }

    @Test
    public void addSerialNumber(){
        for(int i = 0; i< 10;i ++ ){
            final SerialNumber number = new SerialNumber();
            number.setCustomerId((long) 1111111111);
            number.setSerialNumber(String.valueOf(100101+i));
            mSerialNumberMapper.insertSelective(number);
        }
    }

    @Test
    public void addOrderConfig(){
        final OrderConfig config = new OrderConfig();
        config.setTagName("first");
        config.setTagValue("第一次下单");
        mOrderConfigMapper.insertSelective(config);
    }

    @Test
    public void getSerialNumberByCustomerId(){
        final List<String> numbers = mProductToolsService.getSerialNumberByCustomerId(Long.parseLong("1111111111"));
        for(String number:numbers){
            System.out.println(number);
        }
    }

    @Test
    public void updateByPrimaryKeySelective(){
        int result = mProductToolsService.updateByPrimaryKeySelective(Long.parseLong("1111111111"),"100101");
        System.out.println("返回结果为--->" + result);
    }
}
