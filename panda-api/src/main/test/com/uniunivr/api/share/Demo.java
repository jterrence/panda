package com.uniunivr.api.share;


/**
 * Created by terrence on 17-4-9.
 */
public class Demo {

    public static final void main(String[] args){
        final String[] commands     = new String[]{
                "./krpanotools",
                "makepano",
                "-config=templates/vtour-multires.config",
                "test.jpg"
        };
        final ProcessBuilder builder = new ProcessBuilder(commands);
        ProcessBuilderUtils.executeCommand(builder,true);
    }
}
