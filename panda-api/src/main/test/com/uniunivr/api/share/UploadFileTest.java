package com.uniunivr.api.share;

import com.uniunivr.api.controller.HtmlUtils;
import com.uniunivr.api.controller.PanoUtils;
import com.uniunivr.api.service.impl.PanoramaServiceImpl;
import com.uniunivr.common.config.PandaConfig;
import com.uniunivr.common.util.FFmpegCommandUtils;
import com.uniunivr.common.util.KrpanoCommandUtils;
import com.uniunivr.common.util.ThumbnailUtils;

import java.io.File;
import java.io.IOException;
import java.io.SyncFailedException;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by terrence on 17-3-25.
 */
public class UploadFileTest {
    public static void main(String[] args){
//        FFmpegCommandUtils.separateAudioFromMp4("/usr/local/ffmpeg/bin/ffmpeg","/Users/terrence/Downloads/1489061946_VIDEO.mp4",
//                "/Users/terrence/Downloads/1489061946_VIDEO.mp3",true);
//        FFmpegCommandUtils.screenshot("/usr/local/ffmpeg/bin/ffmpeg",
//                "/Users/terrence/Downloads/1489061946_VIDEO.mp4",
//                "/Users/terrence/Downloads/1489061946_VIDEO.jpg",
//                "1920x960",
//                true);
//        FFmpegCommandUtils.convertResolution("/usr/local/ffmpeg/bin/ffmpeg",
//                "/Users/terrence/Downloads/1489061946_VIDEO.mp4",
//                "/Users/terrence/Downloads/1024X512.mp4",
//                "1024X512",
//                true);

        KrpanoCommandUtils.makeCubeImage("/Users/terrence/Desktop/krpano-1.19-pr8/krpanotools",
                "/Users/terrence/Downloads/15.jpg",true);

//        final LinkedList<String> options = KrpanoCommandUtils.buildVtourCustomOptions(
//                PandaConfig.getStaticUrl() + "image/",
//                "/Users/terrence/Documents/DevTool/share/scenes/",
//                 "15"
//
//        );

//        KrpanoCommandUtils.makeVtourImage("/Users/terrence/Documents/DevTool/Krpano/krpano-1.19-pr8/krpanotools",
//        "/Users/terrence/Documents/DevTool/share/image/15.jpg",options, true);
//
//
//        HtmlUtils.transformHtmlFile("/Users/terrence/Documents/DevTool/Krpano/krpano-1.19-pr8/templates/html/photo.html",
//        "/Users/terrence/Documents/DevTool/share/p/15","我是司机五号");
//        final String fileName ="/Users/terrence/Downloads/11.jpg";
//
//        System.out.println(fileName.substring(0,fileName.lastIndexOf("/") + 1) );
//
//        System.out.println(fileName.substring(fileName.lastIndexOf("/") + 1,fileName.length()));

//        KrpanoCommandUtils.makeVtourImage("/Users/terrence/Documents/DevTool/Krpano/krpano-1.19-pr8/krpanotools",
//                "/Users/terrence/Desktop/pano/1489061946_VIDEO.mp4",KrpanoCommandUtils.buildVtourPageInfo(
//                        "司机、司机",
//                        "2017-03-09"
//                ),true);

//        HtmlUtils.transformHtmlFile("/Users/terrence/Documents/DevTool/Krpano/krpano-1.19-pr8/templates/html/video.html",
//                "/Users/terrence/Documents/DevTool/apache-tomcat-7.0.73/webapps/share/p/16","我是司机五号");

//        PanoramaServiceImpl panorama = new PanoramaServiceImpl();
//
//        final File file = new File("/Users/terrence/Downloads/1489061946_VIDEO.mp4");
//        panorama.makeVtourVideo(
//                "/usr/local/ffmpeg/bin/ffmpeg",
//                "/Users/terrence/Documents/DevTool/share/video/1489061946_VIDEO.mp4",
//                 "/Users/terrence/Documents/DevTool/share/video/public/media/mp4/",
//                "1920x960",
//                "1024x512",
//                true
//        );
//

//        HtmlUtils.transformHtmlFile(
//                "/Users/terrence/Documents/DevTool/Krpano/krpano-1.19-pr8/templates/html/video.html",
//                "/Users/terrence/Documents/DevTool/share/p/" + getFileSuffix(file.getName()),"我是司机六号");


//        try {
//            final File outPath = new File("/Users/terrence/Downloads/thumb/11.jpg");
//
//            long startTime     = System.currentTimeMillis();
//            ThumbnailUtils.thumbnail("/Users/terrence/Downloads/11.jpg",
//                    outPath.getAbsolutePath(),300,300);
//            long endTime       = System.currentTimeMillis();
//
//            System.out.println("需要花费时间--->" + (endTime - startTime) / 1000);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        final String fileName = "/Users/terrence/Downloads/11.jpg";

        System.out.println(fileName.substring(fileName.lastIndexOf("/")+ 1,fileName.length()));

        final File file = new File("/var/lib/tomcat7/webapps/panda-share/1111111/");

        File file1 = new File(file,"111");

    }

    //获取文件前缀
    private static final String getFileSuffix(String fileName){
        final String suffix  = fileName.
                substring(0,fileName.lastIndexOf("."));
        return suffix;
    }
}
