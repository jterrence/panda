package com.uniunivr.api.model;

/**
 * @描述：     @添加分享
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class AddShare {

    private String shareUrl;

    private String thumbUrl;

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }
}
