package com.uniunivr.api.model;

/**
 * @描述：     @检查版本响应实体类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class CheckVersion {

    private boolean isUpgrade;
    private boolean isPublish;
    private boolean isRecommend;
    private String  downloadPrefixUrl;
    private String  downloadSuffixUrl;
    private String  fileSize;
    private String  fileName;
    private String  newVersionName;
    private String  updateContent;

    public boolean isUpgrade() {
        return isUpgrade;
    }

    public void setUpgrade(boolean upgrade) {
        isUpgrade = upgrade;
    }

    public boolean isPublish() {
        return isPublish;
    }

    public void setPublish(boolean publish) {
        isPublish = publish;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getNewVersionName() {
        return newVersionName;
    }

    public void setNewVersionName(String newVersionName) {
        this.newVersionName = newVersionName;
    }

    public String getDownloadPrefixUrl() {
        return downloadPrefixUrl;
    }

    public void setDownloadPrefixUrl(String downloadPrefixUrl) {
        this.downloadPrefixUrl = downloadPrefixUrl;
    }

    public String getDownloadSuffixUrl() {
        return downloadSuffixUrl;
    }

    public void setDownloadSuffixUrl(String downloadSuffixUrl) {
        this.downloadSuffixUrl = downloadSuffixUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUpdateContent() {
        return updateContent;
    }

    public void setUpdateContent(String updateContent) {
        this.updateContent = updateContent;
    }

    public boolean isRecommend() {
        return isRecommend;
    }

    public void setRecommend(boolean recommend) {
        isRecommend = recommend;
    }
}
