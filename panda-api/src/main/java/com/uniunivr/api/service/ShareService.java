package com.uniunivr.api.service;


import com.uniunivr.api.pojo.Share;
import com.uniunivr.common.generic.GenericService;

/**
 * @描述：     @业务接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */

public interface ShareService extends GenericService<Share,Long>{

    /**
     * 获取分享id
    * @return
     */
    Long getPrimaryKey();

    /**
     * 更新分享喜欢数量
     * @param postId
     * @return
     */
    int updateLikeCount(String postId);

    /**
     * 更新分享内容
     * @param postId
     * @return
     */
    int updateBrowseCount(String postId);
    /**
     * 获取浏览数量
     * @param shareId
     * @return
     */
    int getBrowseCount(Long shareId);

    /**
     * 获取喜欢数量
     * @param shareId
     * @return
     */
    int getLikeCount(Long shareId);

    /**
     * 获取
     * @return
     */
    Share getByPostId(String postId);

}
