package com.uniunivr.api.service;

import com.uniunivr.common.generic.GenericService;
import com.uniunivr.common.model.ResultModel;

import java.io.IOException;

/**
 * @描述：     @全景图片、全景视频业务接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public interface PanoramaService{

    /**
     * 生成全景图片漫游页面
     * @param toolRootDir
     * @param filePath
     * @param fileSuffix
     * @param showLog
     * @return
     */
    String makeVtourImage(String toolRootDir, String filePath,
                          String title,String content,
                          String fileSuffix, boolean showLog) throws IOException;

    /**
     * 处理视频文件
     * @param toolRootDir
     * @param video
     * @param originalResolution
     * @param conversionResolution
     * @param showlog
     */
    String makeVtourVideo(String toolRootDir, String video,
                          String title,
                          String content,
                          String uploadDir,
                          String originalResolution, String conversionResolution, boolean showlog) throws IOException;


    /**
     * 通过命令行注册krpano
     * @param toolRootDir
     * @param key
     * @return
     */
    boolean register(String toolRootDir, String key);


}
