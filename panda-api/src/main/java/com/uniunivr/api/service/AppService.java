package com.uniunivr.api.service;


import com.uniunivr.api.controller.AppConfigUtils;
import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.api.pojo.Share;
import com.uniunivr.common.generic.GenericService;

import java.io.File;

/**
 * @描述：     @App客户端管理接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */

public interface AppService extends GenericService<AppVersion,Long>{
    /**
     * 检查版本更新
     * @param appId
     * @param channelId
     * @param versionCode
     * @param
     */
    AppVersion checkVersion(String appId,int channelId,int versionCode);

    /**
     * 解析apk
     * @param file
     * @return
     */
    AppVersion parserApk(File file);

    /**
     * 更新版本记录
     * @param appVersion
     */
    void updateVersion(AppVersion appVersion);

    /**
     * 通过appid查询此app是否可以运行
     * @param appId
     * @return
     */
    int isRunning(String appId);
}
