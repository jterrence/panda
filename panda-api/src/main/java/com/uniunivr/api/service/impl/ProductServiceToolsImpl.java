package com.uniunivr.api.service.impl;

import com.uniunivr.api.dao.CustomerMapper;
import com.uniunivr.api.dao.OrderConfigMapper;
import com.uniunivr.api.dao.SerialNumberMapper;
import com.uniunivr.api.pojo.SerialNumber;
import com.uniunivr.api.service.ProductToolsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * @描述：     @量产业务接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Service
@Transactional
public class ProductServiceToolsImpl implements ProductToolsService {

    @Resource
    private CustomerMapper      mCustomerMapper;

    @Resource
    private SerialNumberMapper  mSerialNumberMapper;

    @Resource
    private OrderConfigMapper   mOrderConfigMapper;


    @Override
    public List<String> getSerialNumberByCustomerId(Long customerId) {
        return mSerialNumberMapper.selectSerialNumberByCustomerId(customerId);
    }

    @Override
    public int updateByPrimaryKeySelective(Long customerId, String serialNumber) {
        final SerialNumber number = mSerialNumberMapper.selectBySerialNumber(customerId,serialNumber);
        number.setMpFinish((byte) 1);
        return mSerialNumberMapper.updateByPrimaryKeySelective(number);
    }
}
