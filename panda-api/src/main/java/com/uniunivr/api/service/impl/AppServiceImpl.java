package com.uniunivr.api.service.impl;

import com.uniunivr.api.controller.AndroidManifestParser;
import com.uniunivr.api.controller.AppConfigUtils;
import com.uniunivr.api.controller.PanoUtils;
import com.uniunivr.api.dao.AppChannelMapper;
import com.uniunivr.api.dao.AppVersionMapper;
import com.uniunivr.api.dao.ShareMapper;
import com.uniunivr.api.pojo.AppChannel;
import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.api.pojo.Share;
import com.uniunivr.api.service.AppService;
import com.uniunivr.api.service.ShareService;
import com.uniunivr.common.generic.GenericDao;
import com.uniunivr.common.generic.GenericServiceImpl;
import net.dongliu.apk.parser.ApkFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.ejb.Local;
import java.io.File;
import java.math.BigDecimal;
import java.nio.channels.Channel;
import java.util.Locale;


/**
 * @描述：     @App客户端管理接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppServiceImpl extends GenericServiceImpl<AppVersion,Long> implements AppService {

    @Resource
    private AppVersionMapper mVersionMapper;

    @Resource
    private AppChannelMapper mChannelMapper;

    @Override
    public AppVersion checkVersion(
            String appId, int channelId, int versionCode) {
        return mVersionMapper.selectByAppIdAndChannelId(appId,channelId);
    }


    @Override
    public int isRunning(String appId) {
        return mVersionMapper.selectAppStatu(appId);
    }

    @Override
    public GenericDao<AppVersion, Long> getDao() {
        return mVersionMapper;
    }

    @Override
    public AppVersion parserApk(File file) {
        try{
            final AppVersion version = new AppVersion();
            final ApkFile apkFile    = new ApkFile(file);
            apkFile.setPreferredLocale(Locale.getDefault());
            version.setAppId(apkFile.getApkMeta().getPackageName());
            version.setVersionName(apkFile.getApkMeta().getVersionName());
            version.setVersionCode(Integer.parseInt(apkFile.
                    getApkMeta().getVersionCode() + ""));
            final AndroidManifestParser parser = new AndroidManifestParser(
                    apkFile.getApkMeta(),apkFile.getManifestXml());
            final String  channelName = parser.getChannel(PanoUtils.getAndroidChannel());
            if(!StringUtils.isEmpty(channelName)){
                final AppChannel channel = mChannelMapper.selectAppChannelByChannId(
                        Integer.parseInt(channelName));
                if(!StringUtils.isEmpty(channel)){
                    version.setChannelId(channel.getChannelId());
                    version.setChannelName(channel.getChannelName());
                }
            }
            version.setChannelId(AppConfigUtils.OFFICIAL_CHANNEL);
            version.setChannelName(AppConfigUtils.getDefaultChannelName());
            version.setFileSize(formatSize(file.length()));
            version.setFileName(file.getName());
            version.setAppName(apkFile.getApkMeta().getName());
            version.setAppIcon(apkFile.getApkMeta().getIcon());
            return version;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public int insert(AppVersion appVersion) {
        return super.insert(appVersion);
    }

    @Override
    public void updateVersion(AppVersion appVersion) {
        final AppVersion oldVersion = mVersionMapper.selectByAppIdAndChannelId(
                appVersion.getAppId(),
                appVersion.getChannelId());
        //如果没有之前的版本记录，刚新增
        if(null != oldVersion){
            final String oldFileName = oldVersion.getFileName();
            oldVersion.setVersionName(appVersion.getVersionName());
            oldVersion.setVersionCode(appVersion.getVersionCode());
            oldVersion.setAppIcon(appVersion.getAppIcon());
            oldVersion.setAppName(appVersion.getAppName());
            oldVersion.setUpdateContent(appVersion.getUpdateContent());
            oldVersion.setFileSize(appVersion.getFileSize());
            oldVersion.setFileName(appVersion.getFileName());
            oldVersion.setCreateTime(appVersion.getCreateTime());
            update(oldVersion);
            final File file = new File(AppConfigUtils.
                    getAndroidApkUploadUrl(),oldFileName);
            if(file.exists()){
                file.delete();
            }
        }else{
           insert(appVersion);
        }
    }

    // 格式化单位
    private static String formatSize(double size) {
        double kiloByte = size / 1024;
        if (kiloByte < 1) {
            return size + "Byte";
        }
        double megaByte = kiloByte / 1024;
        if (megaByte < 1) {
            BigDecimal result1 = new BigDecimal(Double.toString(kiloByte));
            return result1.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "KB";
        }
        double gigaByte = megaByte / 1024;
        if (gigaByte < 1) {
            BigDecimal result2 = new BigDecimal(Double.toString(megaByte));
            return result2.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "MB";
        }
        double teraBytes = gigaByte / 1024;
        if (teraBytes < 1) {
            BigDecimal result3 = new BigDecimal(Double.toString(gigaByte));
            return result3.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "GB";
        }
        BigDecimal result4 = new BigDecimal(teraBytes);
        return result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "TB";
    }
}
