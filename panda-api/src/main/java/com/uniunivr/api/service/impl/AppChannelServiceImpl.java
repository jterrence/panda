package com.uniunivr.api.service.impl;

import com.uniunivr.api.controller.AppConfigUtils;
import com.uniunivr.api.dao.AppChannelMapper;
import com.uniunivr.api.dao.AppVersionMapper;
import com.uniunivr.api.pojo.AppChannel;
import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.api.service.AppChannelService;
import com.uniunivr.api.service.AppService;
import com.uniunivr.common.generic.GenericDao;
import com.uniunivr.common.generic.GenericServiceImpl;
import net.dongliu.apk.parser.ApkFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.math.BigDecimal;
import java.util.Locale;


/**
 * @描述：     @App客户端管理接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppChannelServiceImpl extends GenericServiceImpl<AppChannel,Long> implements AppChannelService {

    @Resource
    private AppChannelMapper mChannelMapper;


    @Override
    public GenericDao<AppChannel, Long> getDao() {
        return mChannelMapper;
    }


    @Override
    public int insert(AppChannel appChannel) {
        return super.insert(appChannel);
    }

    @Override
    public AppChannel getAppChannelByChannId(int channelId) {
        return mChannelMapper.selectAppChannelByChannId(channelId);
    }
}
