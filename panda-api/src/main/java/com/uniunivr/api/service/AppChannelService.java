package com.uniunivr.api.service;


import com.uniunivr.api.pojo.AppChannel;
import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.common.generic.GenericService;

import java.io.File;

/**
 * @描述：     @App渠道管理接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */

public interface AppChannelService extends GenericService<AppChannel,Long>{

    /**
     * 通过渠道id获取渠道信息
     * @param channelId
     * @return
     */
    AppChannel getAppChannelByChannId(int channelId);
}
