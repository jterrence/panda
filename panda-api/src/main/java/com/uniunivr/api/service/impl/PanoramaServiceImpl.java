package com.uniunivr.api.service.impl;

import com.uniunivr.api.controller.HtmlUtils;
import com.uniunivr.api.controller.PanoUtils;
import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.api.service.PanoramaService;
import com.uniunivr.common.config.PandaConfig;
import com.uniunivr.common.generic.GenericDao;
import com.uniunivr.common.generic.GenericServiceImpl;
import com.uniunivr.common.model.ResultModel;
import com.uniunivr.common.util.FFmpegCommandUtils;
import com.uniunivr.common.util.KrpanoCommandUtils;
import com.uniunivr.common.util.ThumbnailUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

/**
 * @描述：     @全景图片、全景视频业务实现类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PanoramaServiceImpl implements PanoramaService {

    public Logger logger         = (Logger) LogManager.
            getLogger(LogManager.ROOT_LOGGER_NAME);

    @Override
    public String makeVtourImage(String toolRootDir, String filePath,
                                 String title, String content,String suffix, boolean showLog) throws IOException {
        final LinkedList<String> options = KrpanoCommandUtils.buildVtourCustomOptions(
                PandaConfig.getStaticUrl(),
                PanoUtils.getMediaJpg(),
                PanoUtils.getMediaThumb(),
                PanoUtils.getUploadImageDir(),
                PanoUtils.getSceneXmlPath(),
                suffix

        );

        //首先要进行注册
        if(register(toolRootDir,PanoUtils.getRegisterKey())){
            logger.info("注册krpano成功");
            //生成全景切割图片
            if(KrpanoCommandUtils.makeVtourImagePage(toolRootDir,
                    filePath,options,true) == 0? true:false){
                HtmlUtils.transformHtmlFile(
                        PanoUtils.getPhotoTemplatePath(),
                        PanoUtils.getSharePath() +
                                suffix,title,content);
                logger.info("生成分享页面成功");
                //缩略图路径
                final String thumbUrl = PanoUtils.getThumbDir() +
                        suffix + "/" + PanoUtils.getWeXinSuffix();

                //生成微信缩略图
                ThumbnailUtils.thumbnail(filePath,thumbUrl,
                        300,300);

                logger.info("生成缩略图成功");

                final File file      = new File(filePath);
                //删除原文件
                if(file.exists()){
                    file.delete();
                }

                //删除父目录
                if(file.getParentFile().exists()){
                    file.getParentFile().delete();
                }
                return thumbUrl;
            }
        }
        return null;
    }



    @Override
    public String makeVtourVideo(String toolRootDir, String video,
                                 String title,
                                 String content,
                                 String uploadDir,
                                 String originalResolution, String conversionResolution, boolean showlog) throws IOException {
        final String fileName     = video.substring(video.lastIndexOf("/") + 1,video.length());
        final String filePrefix   = fileName.substring(0,fileName.indexOf("."));
        final String newFileDir   = uploadDir + filePrefix + "/";

        final File fileDir        = new File(newFileDir);
        if(!fileDir.exists()){
            fileDir.mkdirs();
        }

        final String newFileName  = newFileDir + filePrefix;

       //提取音频
        FFmpegCommandUtils.separateAudioFromMp4(toolRootDir,video,
                newFileName + ".mp3",showlog);

        final String screenShot   = newFileName + "_poster" +originalResolution+ ".jpg";
        final File thumbDir       = new File(PanoUtils.getThumbDir() + filePrefix);
        if(!thumbDir.exists()){
            thumbDir.mkdirs();
        }

        final String thumbUrl     = PanoUtils.getThumbDir() +
                filePrefix + "/" + PanoUtils.getWeXinSuffix();

        //生成源文件截图
        FFmpegCommandUtils.screenshot(toolRootDir,video,
                screenShot,originalResolution,showlog);
        logger.info("生成源文件截图成功");

        final File screenFile     = new File(screenShot);

        if(screenFile.exists()){
            //生成微信缩略图
            ThumbnailUtils.thumbnail(screenShot,thumbUrl,
                    300,300);
            logger.info("生成源文件缩略图成功");
        }

        //转换分辨率
        FFmpegCommandUtils.convertResolution(toolRootDir,video,
                newFileName+ "_" +conversionResolution + ".mp4",conversionResolution,showlog);
        logger.info("转换分辨率" + conversionResolution +"成功");

        //生成转换分辨率文件截图
        FFmpegCommandUtils.screenshot(toolRootDir,video,
                newFileName +
                        "_poster" +conversionResolution+ ".jpg",conversionResolution,showlog);

        logger.info("生成转换分辨率" + newFileName +
                "_poster" +conversionResolution+ ".jpg" +"文件截图成功");

        final File fileVideo     = new File(video);

        if(fileVideo.exists()){
            fileVideo.renameTo(new File(newFileName + "_" + originalResolution + ".mp4"));
            logger.info("重命名文件成功");
        }

        HtmlUtils.transformHtmlFile(
                PanoUtils.getVideoTemplatePath(),
                PanoUtils.getSharePath() + filePrefix,title,content);
        logger.info("生成视频缩略图成功");
        return thumbUrl;
    }

    @Override
    public boolean register(String toolRootDir, String key) {
        return KrpanoCommandUtils.registerKrpanotools(toolRootDir,key,true) == 0? true:false;
    }

}
