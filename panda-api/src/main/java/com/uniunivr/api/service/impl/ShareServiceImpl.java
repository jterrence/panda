package com.uniunivr.api.service.impl;

import com.uniunivr.api.dao.ShareMapper;
import com.uniunivr.api.pojo.Share;
import com.uniunivr.api.service.ShareService;
import com.uniunivr.common.generic.GenericDao;
import com.uniunivr.common.generic.GenericServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * @描述：     @业务实现类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ShareServiceImpl extends GenericServiceImpl<Share,Long> implements ShareService {

    @Resource
    private ShareMapper mShareMapper;

    @Override
    public Long getPrimaryKey() {
        return mShareMapper.selectPrimaryKey() + 1;
    }

    @Override
    public GenericDao<Share, Long> getDao() {
        return mShareMapper;
    }

    @Override
    public int insert(Share share) {
        return super.insert(share);
    }

    @Override
    public int updateLikeCount(String postId) {
        final Share share     = mShareMapper.selectByPostId(postId);
        int likeCount         = 0;
        if(null != share){
            likeCount         = share.getLikeCount() + 1;
            share.setLikeCount(likeCount);
            mShareMapper.updateLikeCount(share);
        }
        return likeCount;
    }

    @Override
    public int updateBrowseCount(String postId) {
        Share share           = mShareMapper.selectByPostId(postId);
        int browseCount       = 0;
        if(null != share){
            browseCount       = share.getBrowseCount() + 1;
            share.setBrowseCount(browseCount);
            mShareMapper.updateBrowseCount(share);
        }
        return browseCount;
    }

    @Override
    public int getBrowseCount(Long shareId) {
        return mShareMapper.selectBrowseCount(shareId);
    }

    @Override
    public int getLikeCount(Long shareId) {
        return mShareMapper.selectLikeCount(shareId);
    }

    @Override
    public Share getByPostId(String postId) {
        return mShareMapper.selectByPostId(postId);
    }

}
