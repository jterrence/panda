package com.uniunivr.api.service;

import com.uniunivr.api.pojo.SerialNumber;

import java.util.List;

/**
 * @描述：     @量产业务接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public interface ProductToolsService {

    /**
     * 获取客户机器序列号
     * @param customerId
     */
    List<String> getSerialNumberByCustomerId(Long customerId);

    /**
     * 标记量产已完成
     * @param customerId
     * @param serialNumber
     */
    int updateByPrimaryKeySelective(Long customerId,String serialNumber);
}
