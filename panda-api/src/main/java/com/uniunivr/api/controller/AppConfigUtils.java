package com.uniunivr.api.controller;


import com.uniunivr.common.property.CustomizedPropertyConfigurer;

/**
 * @描述：     @app相关工具类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class AppConfigUtils {



    private static final String ANDROID_TYPE  = "AndroidType";

    private static final String IPHONE_TYPE   = "iphoneType";

    /**
     * 官网
     */
    public static final int  OFFICIAL_CHANNEL = 1;

    /**
     * 获取安卓apk上传目录
     * @return
     */
    public static final String getAndroidApkUploadUrl(){
        return CustomizedPropertyConfigurer.getProperty("AndroidApkUploadUrl");
    }

    /**
     * 安卓apk下载目录前缀
     * @return
     */
    public static final String getDownloadAndroidApkPrefixUrl(){
        return CustomizedPropertyConfigurer.getProperty("DownloadAndroidApkPrefixUrl");
    }

    /**
     * 安卓apk下载目录后缀
     * @return
     */
    public static final String getDownloadAndroidApkSuffixUrl(){
        return CustomizedPropertyConfigurer.getProperty("DownloadAndroidApkSuffixUrl");
    }

    /**
     * 获取默认官网渠道
     * @return
     */
    public static final String getDefaultChannelName(){
        return CustomizedPropertyConfigurer.getProperty("OfficialChannel");
    }

    /**
     * 获取Android类型
     * @return
     */
    public static final String getAndroidType(){
        return CustomizedPropertyConfigurer.getProperty(ANDROID_TYPE);
    }

    /**
     * 获取iphone类型
     * @return
     */
    public static final String getIphoneType(){
        return CustomizedPropertyConfigurer.getProperty(IPHONE_TYPE);
    }
}
