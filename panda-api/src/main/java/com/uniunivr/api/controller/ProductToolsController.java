package com.uniunivr.api.controller;

import com.uniunivr.api.service.ProductToolsService;
import com.uniunivr.common.controller.BaseController;
import com.uniunivr.common.model.ResultModel;
import com.uniunivr.common.util.ResponseUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @描述：     @量产工具完成业务控制类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Controller
@RequestMapping("/product")
public class ProductToolsController extends BaseController{

    @Resource
    private ProductToolsService mProductTools;

    @RequestMapping(value = "/getSerialNumber")
    public void getSerialNumber(
            final HttpServletRequest request, HttpServletResponse response) throws IOException {
        final Long customerId = getLongParameter("customerId");
        final ResultModel<List<String>> result= new ResultModel<>();
        if(!StringUtils.isEmpty(customerId)){
            List<String> numbers = null;
            try{
                numbers   = mProductTools.getSerialNumberByCustomerId(customerId);
                result.setData(numbers);
                obtainResponseMessage(result,ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
            }catch (Exception e){
                obtainResponseMessage(result,ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
            }
        }else{
            obtainResponseMessage(result, ResponseUtils.RESPONSE_PANDA_FAILURE_PARAM_ERROR);
        }
        write(response,toJSON(result));
    }


    @RequestMapping(value = "/markFinish")
    public void markFinish(
            final HttpServletRequest request, HttpServletResponse response) throws IOException {
        final Long customerId       = getLongParameter("customerId");
        final String serialNumber   = getStringParameter("serialNumber");
        final ResultModel result    = new ResultModel();
        if(!StringUtils.isEmpty(customerId)
                && !StringUtils.isEmpty(serialNumber)){
            try{
                if(mProductTools.updateByPrimaryKeySelective(customerId,serialNumber) > 0){
                    obtainResponseMessage(result,ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
                }else{
                    obtainResponseMessage(result,ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
                }
            }catch (Exception e){
                obtainResponseMessage(result,ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
            }
        }else{
            obtainResponseMessage(result,ResponseUtils.RESPONSE_PANDA_FAILURE_PARAM_ERROR);
        }
        write(response,toJSON(result));
    }

}
