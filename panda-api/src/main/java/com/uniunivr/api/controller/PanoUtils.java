package com.uniunivr.api.controller;


import com.uniunivr.common.property.CustomizedPropertyConfigurer;

/**
 * @描述：     @全景相关工具类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class PanoUtils {

    /**
     * 获取krpanoToolDir工具目录
     * @return
     */
    public static final String getKrpanoToolsDir(){
        return CustomizedPropertyConfigurer.getProperty("krpanoToolsDir");
    }

    /**
     * 获取图片上传目录
     * @return
     */
    public static final String getUploadImageDir(){
        return CustomizedPropertyConfigurer.getProperty("uploadImageDir");
    }

    /**
     * 获取ffmpeg根目录
     * @return
     */
    public static final String getFFmpegRootDir(){
        return CustomizedPropertyConfigurer.getProperty("ffmpegRootDir");
    }
    /**
     * 获取视频上传目录
     * @return
     */
    public static final String getUploadVideoDir(){
        return CustomizedPropertyConfigurer.getProperty("uploadVideoDir");
    }

    /**
     * 获取源视频文件分辨率
     * @return
     */
    public static final String getOriginalResolution(){
        return CustomizedPropertyConfigurer.getProperty("originalResolution");
    }

    /**
     * 获取转换后视频文件分辨率
     * @return
     */
    public static final String getConversionResolution(){
        return CustomizedPropertyConfigurer.getProperty("conversionResolution");
    }

    /**
     * 获取全景图片页面模板路径
     * @return
     */
    public static final String getPhotoTemplatePath(){
        return CustomizedPropertyConfigurer.getProperty("photoTemplateHtml");
    }

    /**
     * 获取全景视频页面模板路径
     * @return
     */
    public static final String getVideoTemplatePath(){
        return CustomizedPropertyConfigurer.getProperty("videoTemplateHtml");
    }

    /**
     * 获取由模板生成的html路径
     * @return
     */
    public static final String getSharePath(){
        return CustomizedPropertyConfigurer.getProperty("sharePath");
    }


    /**
     * 获取输出场景xml文件
     * @return
     */
    public static final String getSceneXmlPath(){
        return CustomizedPropertyConfigurer.getProperty("sceneXmlPath");
    }


    /**
     * 获取微信命令前缀
     * @return
     */
    public static final String getWeXinSuffix(){
        return CustomizedPropertyConfigurer.getProperty("wexinThumbSuffix");
    }

    /**
     * 获取缩略图dir
     * @return
     */
    public static final String getThumbDir(){
        return CustomizedPropertyConfigurer.getProperty("uploadThumbDir");
    }

    /**
     * 获取缩略图相对路径
     * @return
     */
    public static final String getThumbRelativeDir(){
        return CustomizedPropertyConfigurer.getProperty("uploadThumbRelativeDir");
    }

    /**
     * 获取jpg前缀
     * @return
     */
    public static final String getMediaJpg(){
        return CustomizedPropertyConfigurer.getProperty("mediaJpg");
    }

    /**
     * 获取缩略图前缀
     * @return
     */
    public static final String getMediaThumb(){
        return CustomizedPropertyConfigurer.getProperty("mediaThumb");
    }

    /**
     * 获取注册码
     * @return
     */
    public static final String getRegisterKey(){
        return CustomizedPropertyConfigurer.getProperty("KrpanoRegisterKey");
    }

    /**
     * 获取渠道名称
     * @return
     */
    public static final String getAndroidChannel(){
        return CustomizedPropertyConfigurer.getProperty("androidChannel");
    }
}
