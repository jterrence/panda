package com.uniunivr.api.controller;

import org.springframework.util.StringUtils;

import java.io.*;
import java.util.Date;

/**
 * @描述：     @模板页面转化工具类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class HtmlUtils {

    /**
     * 全景页面视频浏览模板页面
     * @param inputFile
     * @param outFile
     * @return
     */
    public static boolean transformHtmlFile(String inputFile, String outFile,String title,String content) {
        String str            = "";
        long beginDate        = (new Date()).getTime();
        try {
            String tempStr    = "";
            FileInputStream is= new FileInputStream(inputFile);//读取模块文件
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            while ((tempStr   = br.readLine()) != null) {
                str = str + tempStr;
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        try {
            if(StringUtils.isEmpty(content)){
                content      = "";
            }

            if(StringUtils.isEmpty(title)){
                title       = "";
            }

            str              = str.replaceAll("###content###",content);
            str              = str.replaceAll("###shareTitle###",title);
            File f           = new File(outFile);
            if(!f.getParentFile().exists()){
                f.getParentFile().mkdirs();
            }
            BufferedWriter o = new BufferedWriter(new FileWriter(f));
            o.write(str);
            o.close();
            System.out.println("共用时：" + ((new Date()).getTime() - beginDate) + "ms");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
