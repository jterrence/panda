package com.uniunivr.api.controller;

import com.uniunivr.api.model.CheckVersion;
import com.uniunivr.api.pojo.AppChannel;
import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.api.service.AppChannelService;
import com.uniunivr.api.service.AppService;
import com.uniunivr.common.controller.BaseController;
import com.uniunivr.common.model.ResultModel;
import com.uniunivr.common.util.ResponseUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @描述：     @app管理、包括升级、上传app版本
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Controller
@RequestMapping("/app")
public class AppController extends BaseController{

    @Resource
    private AppService mAppService;

    @Resource
    private AppChannelService mAppChannelService;


    @RequestMapping(value = "/isRunning",method = RequestMethod.POST)
    public void isRunning(final HttpServletRequest request,
                          final HttpServletResponse response) throws IOException {
        logger.info("检查app是否可运行状态");
        final ResultModel result               = new ResultModel();
        try{
            final String appId                 = getStringParameter("appId");
            if(!StringUtils.isEmpty(appId)){
                final int isRunning            = mAppService.isRunning(appId);
                final Boolean    b             = new Boolean(isRunning == TRUE?true:false);
                setResultCode(ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
                result.setData(b);
                logger.info("当前app状态为-->" + b.booleanValue());
            }else{
                setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_PARAM_ERROR);
            }
        }catch (Exception e){
            setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
            logger.error("检查app运行状态发生异常--->" + e.getMessage());
        }finally {
            logger.info("响应客户端--->" + getResultCode());
            obtainResponseMessage(result,getResultCode());
            write(response,toJSON(result));
        }
    }

    @RequestMapping(value = "/checkVersion")
    public void checkVersion(final HttpServletRequest request,
                             final HttpServletResponse response) throws IOException {
        final ResultModel<CheckVersion> result = new ResultModel<>(new CheckVersion());
        logger.info("检查版本更新");
        try{
            final String appId                 = getStringParameter("appId");
            final int channelId                = getIntegerParameter("channelId");
            final int versionCode              = getIntegerParameter("versionCode");
            final String versionName           = getStringParameter("versionName");
            boolean update                     = false;
            setResultCode(ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
            if(!StringUtils.isEmpty(appId)
                    && !StringUtils.isEmpty(channelId)
                    && !StringUtils.isEmpty(versionCode)
                    && !StringUtils.isEmpty(versionName)){
                final AppVersion version       = mAppService.checkVersion(
                        appId,channelId,versionCode);

                if(!isEmpty(version)){
                    if(version.getVersionCode() > versionCode
                            && !versionName.equals(
                                    version.getVersionName())) {
                        if(!isEmpty(channelId)){
                            AppChannel channel = mAppChannelService.
                                    getAppChannelByChannId(channelId);
                            if(!isEmpty(channel)){
                                result.getData().setDownloadPrefixUrl(
                                        channel.getDownloadPrefixUrl());
                                result.getData().setDownloadSuffixUrl(
                                        channel.getDownloadSuffixUrl());
                            }
                        }
                        result.getData().setFileSize(
                                version.getFileSize());
                        result.getData().setFileName(
                                version.getFileName());
                        result.getData().setPublish(
                                version.getIsPublish() == TRUE ?true:false);
                        result.getData().setRecommend(version.
                                getUpgradeMode() == TRUE ?true:false);
                        result.getData().setNewVersionName(version.getVersionName());
                        result.getData().setFileSize(version.getFileSize());
                        result.getData().setUpgrade(true);
                        result.getData().setUpdateContent(
                                version.getUpdateContent());

                        update = true;
                        logger.info("检查到有新版本 --> " + update);
                    }
                }
            }else{
                setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_PARAM_ERROR);
            }

            if(!update){
                result.getData().setUpgrade(false);
                result.getData().setPublish(false);
            }

        }catch (Exception e){
            setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
            logger.error("检查版本更新发生异常--->" + e.getMessage());
        }finally {
            logger.info("响应客户端--->" + getResultCode());
            obtainResponseMessage(result,getResultCode());
            write(response,toJSON(result));
        }
    }

    @RequestMapping(value = "/uploadApk")
    public void uploadApk(final HttpServletRequest request,final HttpServletResponse response) throws IOException{
        logger.info("接收到文件上传请求");
        ResultModel result                       = new ResultModel();
        final ExecutorService service            = Executors.newSingleThreadExecutor();
        try{
            final Future<ResultModel> future     = service.submit(new Callable<ResultModel>() {
                @Override
                public ResultModel call() throws Exception {
                    final ResultModel model      = new ResultModel();
                    CommonsMultipartResolver multipartResolver= new CommonsMultipartResolver(
                            request.getServletContext());
                    boolean   success            = false;
                    if(multipartResolver.isMultipart(request)){
                        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
                        //获取multiRequest 中所有的文件名
                        final Iterator iter      = multiRequest.getFileNames();
                        boolean hasNext          = iter.hasNext();
                        if(!hasNext){
                            logger.info("请求参数错误");
                            obtainResponseMessage(model,ResponseUtils.RESPONSE_PANDA_FAILURE_PARAM_ERROR);
                            return model;
                        }

                        while (hasNext){
                            MultipartFile file   = multiRequest.getFile(iter.next().toString());
                            final File oriFile   = new File(AppConfigUtils.getAndroidApkUploadUrl(),
                                    file.getOriginalFilename());

                            final String fileName= oriFile.getName();
                            if(!fileName.endsWith(".apk")
                                    || fileName.endsWith(".ipa")){
                                logger.info("上传文件格式错误--->" + fileName);
                                obtainResponseMessage(model,
                                        ResponseUtils.RESPONSE_PANDA_FILE_FORMAT_ERROR);
                                return model;
                            }

                            logger.info("上传文件到指定的目录--->" + oriFile.getAbsolutePath());
                            if(!oriFile.getParentFile().exists()){
                                oriFile.getParentFile().mkdirs();
                            }

                            file.transferTo(oriFile);

                            logger.info("上传文件完成");
                            if(oriFile.exists()){
                                logger.info("文件存在，刚代表着上传成功，开始操作数据库");
                               final AppVersion appVersion = mAppService.parserApk(oriFile);
                               if(null != appVersion){
                                   if(oriFile.getName().endsWith(".apk")){
                                       appVersion.setAppType(AppConfigUtils.getAndroidType());
                                   }else{
                                       appVersion.setAppType(AppConfigUtils.getIphoneType());
                                   }
                                   appVersion.setIsDelete(FALSE);
                                   appVersion.setIsPublish(FALSE);
                                   appVersion.setCreateTime(new Date());
                                   mAppService.updateVersion(appVersion);
                                   success = true;
                                   logger.info("更新数据库成功");
                               }
                            }
                            hasNext = iter.hasNext();
                        }
                    }

                    if(success){
                        setResultCode(ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
                    }else{
                        setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
                    }
                    obtainResponseMessage(model,ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
                    return model;
                }
            });
            result                               = future.get();
        }catch (Exception e){
            logger.error("上传文件发生异常--->" + e.getMessage());
            setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
        }finally {
            logger.info("响应客户端信息--> " + getResultCode());
            obtainResponseMessage(result,getResultCode());
            write(response,toJSON(result));
        }

        if(!service.isShutdown()){
            logger.info("关闭线程连接池");
            service.isShutdown();
        }
    }

}
