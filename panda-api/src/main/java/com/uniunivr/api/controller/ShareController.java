package com.uniunivr.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.uniunivr.api.model.AddShare;
import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.api.pojo.Share;
import com.uniunivr.api.service.PanoramaService;
import com.uniunivr.api.service.ShareService;
import com.uniunivr.common.config.PandaConfig;
import com.uniunivr.common.controller.BaseController;
import com.uniunivr.common.model.ResultModel;
import com.uniunivr.common.util.DateUtils;
import com.uniunivr.common.util.FFmpegCommandUtils;
import com.uniunivr.common.util.ProcessBuilderUtils;
import com.uniunivr.common.util.ResponseUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.springframework.stereotype.Controller;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @描述：     @分享控制器
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Controller
@RequestMapping("/share")
public class ShareController extends BaseController{

    /**
     * 图片类型
     */
    private final static String FILE_TYPE_PHOTO = "photo";

    /**
     * 视频类型
     */
    private final static String FILE_TYPE_VIDEO = "video";

    /**
     * 上传最大大小10M
     */
    private final static int IMAGE_MAX_SIZE     = 10 * 1024 * 1024;

    /**
     * 视频最大大小300M
     */
    private final static int VIDEO_MAX_SIZE     = 300 * 1024 * 1024;

    @Resource
    private PanoramaService mPanoService;

    @Resource
    private ShareService mShareService;


    @RequestMapping(value = "/addShare",method = RequestMethod.POST)
    public void addShare(final HttpServletRequest request,
                         final HttpServletResponse response) throws IOException {
        logger.info("添加分享开始");
        final ExecutorService service            = Executors.newSingleThreadExecutor();
        final Future<ResultModel<AddShare>> future   = service.submit(new Callable<ResultModel<AddShare>>() {
            @Override
            public ResultModel<AddShare> call() throws Exception {
                long  startTime                  = System.currentTimeMillis();
                final String content             = request.getParameter("content");
                final String title               = request.getParameter("title");
                final String appId               = request.getParameter("appId");
                final String fileTime            = request.getParameter("fileTime");
                final ResultModel<AddShare> model= new ResultModel<AddShare>(new AddShare());

                if(isEmpty(title) || isEmpty(appId) || isEmpty(fileTime)){
                    logger.info("参数异常 title -- > " + title + ", appId -- > " + appId + ",fileTime --->" + fileTime);
                    obtainResponseMessage(model,ResponseUtils.RESPONSE_PANDA_FAILURE_PARAM_ERROR);
                    return model;
                }

                CommonsMultipartResolver multipartResolver= new CommonsMultipartResolver(
                        request.getServletContext());


                //检查form中是否有enctype="multipart/form-data"
                if(multipartResolver.isMultipart(request)){
                    //将request变成多部分request
                    MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
                    //获取multiRequest 中所有的文件名
                    final Iterator iter           = multiRequest.getFileNames();

                    while(iter.hasNext()) {
                        //一次遍历所有文件
                        MultipartFile file        = multiRequest.getFile(iter.next().toString());
                        logger.info("开始遍历文件......");
                        if(!StringUtils.isEmpty(file)) {
                            String orifilename    = file.getOriginalFilename();
                            int rtn               = ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE;
                            //获取扩展名
                            String extensionName  = orifilename.substring(orifilename.lastIndexOf(".")+1);

                            String path           = null;

                            String fileType       = null;

                            boolean executeUpload = false;

                            final String fileName = System.currentTimeMillis() / 1000 +
                                    (extensionName.indexOf(".") > -1?extensionName:"." +extensionName);

                            String thumbUrl       = null;

                            String suffix         = null;
                            //判断是否可支持的图片类型
                            if (getSupportImageFileTypes().contains(extensionName.toLowerCase()))  {
                                fileType          = FILE_TYPE_PHOTO;
                                if(file.getSize() <= IMAGE_MAX_SIZE){
                                    executeUpload = true;
                                    String  dir   = PanoUtils.getUploadImageDir() + getFileSuffix(fileName) + "/";
                                    File dirFile  = new File(dir);
                                    if(!dirFile.exists()){
                                        dirFile.mkdirs();
                                    }
                                    path          = dir + fileName;
                                }else{
                                    rtn           = ResponseUtils.RESPONSE_PANDA_IMAGE_MAX_SIZE_LIMIT;
                                }
                            }

                            //如果不是文件类型、继续判断是否是视频类型
                            if(StringUtils.isEmpty(fileType)){
                                if (getSupportVideoFileTypes().contains(extensionName.toLowerCase()))  {
                                    fileType       = FILE_TYPE_VIDEO;
                                    if(file.getSize() <= VIDEO_MAX_SIZE){
                                        executeUpload = true;
                                        String  dir= PanoUtils.getUploadVideoDir() + getFileSuffix(fileName) + "/";
                                        File dirFile  = new File(dir);
                                        if(!dirFile.exists()){
                                            dirFile.mkdirs();
                                        }
                                        path       = dir + fileName;
                                    }else{
                                        rtn        = ResponseUtils.RESPONSE_PANDA_VIDEO_MAX_SIZE_LIMIT;
                                    }
                                }
                            }

                            //如果不是可支持上传的文件类型、刚提示用户上传失败
                            if(StringUtils.isEmpty(fileType)){
                                rtn               = ResponseUtils.RESPONSE_PANDA_FILE_FORMAT_ERROR;
                            }

                            //执行上传
                            if(executeUpload){
                                final File oriFile= new File(path);

                                if(!oriFile.getParentFile().exists()){
                                    oriFile.getParentFile().mkdirs();
                                }

                                logger.info("开始上传文件......");
                                file.transferTo(oriFile);
                                logger.info("上传文件完成......");
                                suffix            = getFileSuffix(oriFile.getName());
                                if(FILE_TYPE_PHOTO.equals(fileType)){
                                    //如果是全景图，刚转换成立方体投影
                                    logger.info("开始生成全景图......");
                                    thumbUrl      = mPanoService.makeVtourImage(
                                            PanoUtils.getKrpanoToolsDir(),path,title,content,suffix,true);
                                    logger.info("生成全景图完成......");
                                }

                                //视频
                                if(FILE_TYPE_VIDEO.equals(fileType)){
                                    logger.info("开始处理视频文件...");
                                    thumbUrl = mPanoService.makeVtourVideo(
                                            PanoUtils.getFFmpegRootDir(),
                                            path,
                                            title,
                                            content,
                                            PanoUtils.getUploadVideoDir(),
                                            PanoUtils.getOriginalResolution(),
                                            PanoUtils.getConversionResolution(),
                                            true
                                    );

                                    logger.info("处理视频完成，返回thumbUrl-->" + thumbUrl);
                                }

                                try{
                                    //保存到数据库
                                    logger.info("开始保存数据库");
                                    if(!StringUtils.isEmpty(thumbUrl)){
                                        final AddShare share = new AddShare();
                                        saveShare(appId,fileType,title,content,PandaConfig.getShareUrl(),oriFile.getName(),
                                                fileTime,
                                                getClientOS(request),getClientVersion(request),
                                                getClientVersionCode(request),getClientMac(request));
                                        share.setShareUrl(PandaConfig.getShareUrl() + getFileSuffix(oriFile.getName()));
                                        share.setThumbUrl(getThumbUrl(thumbUrl,suffix));
                                        model.setData(share);
                                        rtn           = ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE;
                                    }else{
                                        rtn           = ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE;
                                    }
                                    logger.info("保存数据到库成功");
                                }catch (Exception e){
                                    rtn               = ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE;
                                    logger.error("插入分享记录到数据库失败-->>" + e.getMessage());
                                }
                            }
                            obtainResponseMessage(model,rtn);
                        }
                    }

                }else{
                    obtainResponseMessage(model, ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
                }
                long  endTime = System.currentTimeMillis();
                logger.info("上传文件花费时间--->" + (endTime - startTime) / 1000 + " 秒");
                return model;
            }
        });

        ResultModel<AddShare> result = null;
        try {
            result       = future.get();
        } catch (Exception e) {
            result        = new ResultModel<AddShare>();
            obtainResponseMessage(result,ResponseUtils.
                    RESPONSE_PANDA_FAILURE_STATUS_CODE);
            logger.error("执行分享失败-->>" + e.getMessage());
        }
        write(response,toJSON(result));
        if(!service.isShutdown()){
            service.shutdown();
        }
    }


    /**
     * 拼接缩略图地址
     * @param thumbUrl
     * @return
     */
    private final String getThumbUrl(String thumbUrl,String suffix){
        final String tempFileName = thumbUrl.substring(thumbUrl.lastIndexOf("/")+ 1,thumbUrl.length());
        return PandaConfig.getStaticUrl() + PanoUtils.getThumbRelativeDir() + suffix + "/" + tempFileName;
    }



    //保存分享记录
    private final void saveShare(String appId,String shareType,
                                 String title,
                                 String content,
                                 String shareUrl,String fileName,
                                 String fileTime,
                                 String clientOS, String clientVersion,
                                 String clientVersionCode, String mac) throws Exception{
        String postId     = getFileSuffix(fileName);
        final Share share = new Share();
        share.setUserId("0000");
        share.setPostId(postId);
        share.setAppId(appId);
        share.setShareType(shareType);
        share.setShareTitle(title);
        share.setShareContent(content);
        share.setShareTime(new Date());
        if(!StringUtils.isEmpty(fileTime)){
            share.setFileTime(DateUtils.longToDate(Long.parseLong(fileTime)));
        }
        share.setFilename(fileName);
        share.setShareUrl(shareUrl + postId);
        share.setBrowseCount(0);
        share.setLikeCount(0);
        share.setClientOs(clientOS);
        share.setClientVersion(clientVersion);
        share.setClientVersionCode(clientVersionCode);
        share.setMac(mac);
        share.setIsDelete(0);
        mShareService.insert(share);
    }

    @RequestMapping(value = "/getShareInfo")
    public void getShareInfo(HttpServletRequest request,
                             HttpServletResponse response)  throws IOException {
        final String  postId = getStringParameter("postId");
        if(!StringUtils.isEmpty(postId)){
            try{
                final Share share   = mShareService.getByPostId(postId);
                write(response,toJSON(share));
            }catch (Exception e){

            }
        }
    }

    @RequestMapping(value = "/getBrowseCount")
    public void getBrowseCount(HttpServletRequest request,
                               HttpServletResponse response)  throws IOException {
        final Long shareId  = getLongParameter("shareId");
        if(!StringUtils.isEmpty(shareId)){
            JSONObject object   = new JSONObject();
            object.put("browseCount",mShareService.getBrowseCount(shareId));
            write(response,object.toJSONString());
        }
    }

    @RequestMapping(value = "/getLikeCount")
    public void getLikeCount(HttpServletRequest request, HttpServletResponse response)  throws IOException {
        final Long shareId  = getLongParameter("shareId");
        if(!StringUtils.isEmpty(shareId)){
            JSONObject object   = new JSONObject();
            object.put("likeCount",mShareService.getLikeCount(shareId));
            write(response,object.toJSONString());
        }
    }

    @RequestMapping(value = "/updateLikeCount")
    public void updateLikeCount(HttpServletRequest request, HttpServletResponse response)  throws IOException {
        final String  postId = getStringParameter("postId");
        if(!StringUtils.isEmpty(postId)){
            JSONObject object   = new JSONObject();
            object.put("likeCount",mShareService.updateLikeCount(postId));
            write(response,object.toJSONString());
        }
    }

    @RequestMapping(value = "/updateBrowseCount")
    public void updateBrowseCount(HttpServletRequest request, HttpServletResponse response){
        final String  postId = getStringParameter("postId");
        if(!StringUtils.isEmpty(postId)){
            try{
                JSONObject object   = new JSONObject();
                object.put("browseCount",mShareService.updateBrowseCount(postId));
                write(response,object.toJSONString());
            }catch (Exception e){

            }
        }
    }

    //获取可支持图片的文件列表
    private final List<String> getSupportImageFileTypes(){
        final List<String> fileTypes = new ArrayList<String>();
        fileTypes.add("jpg");
        fileTypes.add("jpeg");
        fileTypes.add("bmp");
        fileTypes.add("png");
        return fileTypes;
    }

    //获取视频可支持的文件列表
    private final List<String> getSupportVideoFileTypes(){
        final List<String> fileTypes = new ArrayList<String>();
        fileTypes.add("mp4");
        fileTypes.add("webm");
        return fileTypes;
    }

    //获取文件前缀
    private final String getFileSuffix(String fileName){
        final String suffix  = fileName.
                substring(0,fileName.lastIndexOf("."));
        return suffix;
    }

    @RequestMapping(value = "/ffmpeg",method = RequestMethod.POST)
    public void ffmpeg(final HttpServletRequest request,HttpServletResponse response) throws IOException {
        ResultModel result                       = new ResultModel();
        final ExecutorService service            = Executors.newSingleThreadExecutor();
        try{
            final Future<ResultModel> future     = service.submit(new Callable<ResultModel>() {
                @Override
                public ResultModel call() throws Exception {
                    final ResultModel model      = new ResultModel();
                    CommonsMultipartResolver multipartResolver= new CommonsMultipartResolver(
                            request.getServletContext());
                    boolean   success            = false;
                    if(multipartResolver.isMultipart(request)){
                        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
                        //获取multiRequest 中所有的文件名
                        final Iterator iter      = multiRequest.getFileNames();
                        boolean hasNext          = iter.hasNext();
                        if(!hasNext){
                            logger.info("请求参数错误");
                            obtainResponseMessage(model,ResponseUtils.RESPONSE_PANDA_FAILURE_PARAM_ERROR);
                            return model;
                        }

                        while (hasNext){
                            MultipartFile file   = multiRequest.getFile(iter.next().toString());
                            final File dir       = new File("/Users/terrence/Documents/DevTool/apache-tomcat-7.0.73/webapps/panda-share/media/mp4");
                            final File oriFile   = new File(dir,"video.mp4");

                            logger.info("上传文件到指定的目录--->" + oriFile.getAbsolutePath());
                            if(!oriFile.getParentFile().exists()){
                                oriFile.getParentFile().mkdirs();
                            }

                            file.transferTo(oriFile);
                            logger.info("上传文件完成");
                            FFmpegCommandUtils.screenshot(PanoUtils.getFFmpegRootDir(),
                                    oriFile.getAbsolutePath(),
                                    dir.getAbsolutePath() +File.separator+"video.jpg","1920x960",true);
                            hasNext = iter.hasNext();
                            success = true;
                        }
                    }

                    if(success){
                        setResultCode(ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
                    }else{
                        setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
                    }
                    obtainResponseMessage(model,ResponseUtils.RESPONSE_PANDA_SUCCESS_STATUS_CODE);
                    return model;
                }
            });
            result                               = future.get();
        }catch (Exception e){
            logger.error("上传文件发生异常--->" + e.getMessage());
            setResultCode(ResponseUtils.RESPONSE_PANDA_FAILURE_STATUS_CODE);
        }finally {
            logger.info("响应客户端信息--> " + getResultCode());
            obtainResponseMessage(result,getResultCode());
            write(response,toJSON(result));
        }

        if(!service.isShutdown()){
            logger.info("关闭线程连接池");
            service.isShutdown();
        }
    }
}
