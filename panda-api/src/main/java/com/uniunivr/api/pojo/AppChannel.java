package com.uniunivr.api.pojo;

/**
 * @描述:      @app渠道表信息实体类
 * @数据库表：  @TB_CHANNEL
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-06-16
 */
public class AppChannel {

    /**
     * 
     * 表字段 : _ID
     */
    private Long id;

    /**
     * 渠道id
     * 表字段 : CHANNEL_ID
     */
    private Integer channelId;

    /**
     * 渠道名称
     * 表字段 : CHANNEL_NAME
     */
    private String channelName;

    /**
     * 下载链接前缀
     * 表字段 : DOWNLOAD_PREFIX_URL
     */
    private String downloadPrefixUrl;

    /**
     * 下载链接后缀
     * 表字段 : DOWNLOAD_SUFFIX_URL
     */
    private String downloadSuffixUrl;

    /**
     * 获取
     *
     * @return 
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置
     *
     * @param id the value for TB_CHANNEL._ID, 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取渠道id
     *
     * @return 渠道id
     */
    public Integer getChannelId() {
        return channelId;
    }

    /**
     * 设置渠道id
     *
     * @param channelId the value for TB_CHANNEL.CHANNEL_ID, 渠道id
     */
    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    /**
     * 获取渠道名称
     *
     * @return 渠道名称
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * 设置渠道名称
     *
     * @param channelName the value for TB_CHANNEL.CHANNEL_NAME, 渠道名称
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName == null ? null : channelName.trim();
    }

    /**
     * 获取下载链接前缀
     *
     * @return 下载链接前缀
     */
    public String getDownloadPrefixUrl() {
        return downloadPrefixUrl;
    }

    /**
     * 设置下载链接前缀
     *
     * @param downloadPrefixUrl the value for TB_CHANNEL.DOWNLOAD_PREFIX_URL, 下载链接前缀
     */
    public void setDownloadPrefixUrl(String downloadPrefixUrl) {
        this.downloadPrefixUrl = downloadPrefixUrl == null ? null : downloadPrefixUrl.trim();
    }

    /**
     * 获取下载链接后缀
     *
     * @return 下载链接后缀
     */
    public String getDownloadSuffixUrl() {
        return downloadSuffixUrl;
    }

    /**
     * 设置下载链接后缀
     *
     * @param downloadSuffixUrl the value for TB_CHANNEL.DOWNLOAD_SUFFIX_URL, 下载链接后缀
     */
    public void setDownloadSuffixUrl(String downloadSuffixUrl) {
        this.downloadSuffixUrl = downloadSuffixUrl == null ? null : downloadSuffixUrl.trim();
    }
}