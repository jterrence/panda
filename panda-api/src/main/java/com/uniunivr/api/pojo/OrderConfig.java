package com.uniunivr.api.pojo;

/**
 * @描述:      @订单次数配置表信息实体类
 * @数据库表：  @TB_ORDER_CONFIG
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-03-21
 */
public class OrderConfig {

    /**
     * 
     * 表字段 : _ID
     */
    private Long id;

    /**
     * 序列号
     * 表字段 : TAG_NAME
     */
    private String tagName;

    /**
     * {name:value}
     * 表字段 : TAG_VALUE
     */
    private String tagValue;

    /**
     * 获取
     *
     * @return 
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置
     *
     * @param id the value for TB_ORDER_CONFIG._ID, 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取序列号
     *
     * @return 序列号
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * 设置序列号
     *
     * @param tagName the value for TB_ORDER_CONFIG.TAG_NAME, 序列号
     */
    public void setTagName(String tagName) {
        this.tagName = tagName == null ? null : tagName.trim();
    }

    /**
     * 获取{name:value}
     *
     * @return {name:value}
     */
    public String getTagValue() {
        return tagValue;
    }

    /**
     * 设置{name:value}
     *
     * @param tagValue the value for TB_ORDER_CONFIG.TAG_VALUE, {name:value}
     */
    public void setTagValue(String tagValue) {
        this.tagValue = tagValue == null ? null : tagValue.trim();
    }
}