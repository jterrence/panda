package com.uniunivr.api.pojo;

import java.util.Date;

/**
 * @描述:      @客户信息表信息实体类
 * @数据库表：  @TB_CUSTOMER
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-03-21
 */
public class Customer {

    /**
     * 
     * 表字段 : _ID
     */
    private Long id;

    /**
     * 客户标识,11位
     * 表字段 : CUSTOMER_ID
     */
    private Long customerId;

    /**
     * 客户名称
     * 表字段 : CUSTOMER_NAME
     */
    private String customerName;

    /**
     * 创建时间
     * 表字段 : CREATE_TIME
     */
    private Date createTime;

    /**
     * 是否删除，0-未删除；1-已删除
     * 表字段 : IS_DELETE
     */
    private Byte isDelete;

    /**
     * 获取
     *
     * @return 
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置
     *
     * @param id the value for TB_CUSTOMER._ID, 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取客户标识,11位
     *
     * @return 客户标识,11位
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * 设置客户标识,11位
     *
     * @param customerId the value for TB_CUSTOMER.CUSTOMER_ID, 客户标识,11位
     */
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * 设置客户名称
     *
     * @param customerName the value for TB_CUSTOMER.CUSTOMER_NAME, 客户名称
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName == null ? null : customerName.trim();
    }

    /**
     * 获取创建时间
     *
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime the value for TB_CUSTOMER.CREATE_TIME, 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取是否删除，0-未删除；1-已删除
     *
     * @return 是否删除，0-未删除；1-已删除
     */
    public Byte getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除，0-未删除；1-已删除
     *
     * @param isDelete the value for TB_CUSTOMER.IS_DELETE, 是否删除，0-未删除；1-已删除
     */
    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }
}