package com.uniunivr.api.pojo;

import java.util.Date;

/**
 * @描述:      @用户分享表信息实体类
 * @数据库表：  @TB_SHARE
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-06-16
 */
public class Share {

    /**
     * 
     * 表字段 : SHARE_ID
     */
    private Long shareId;

    /**
     * 与shareId组成联合主键
     * 表字段 : POST_ID
     */
    private String postId;

    /**
     * 用户标识
     * 表字段 : USER_ID
     */
    private String userId;

    /**
     * 客户标识,通过包名来区分
     * 表字段 : APP_ID
     */
    private String appId;

    /**
     * 分享类型
     * 表字段 : SHARE_TYPE
     */
    private String shareType;

    /**
     * 分享标题,客户订制
     * 表字段 : SHARE_TITLE
     */
    private String shareTitle;

    /**
     * 分享内容
     * 表字段 : SHARE_CONTENT
     */
    private String shareContent;

    /**
     * 分享地址
     * 表字段 : SHARE_ADDRESS
     */
    private String shareAddress;

    /**
     * 浏览数量
     * 表字段 : BROWSE_COUNT
     */
    private Integer browseCount;

    /**
     * 喜欢数量
     * 表字段 : LIKE_COUNT
     */
    private Integer likeCount;

    /**
     * 分享地址
     * 表字段 : SHARE_URL
     */
    private String shareUrl;

    /**
     * 分享文件名
     * 表字段 : FILENAME
     */
    private String filename;

    /**
     * 来源于哪个操作系统
     * 表字段 : CLIENT_OS
     */
    private String clientOs;

    /**
     * APP版本号
     * 表字段 : CLIENT_VERSION
     */
    private String clientVersion;

    /**
     * APP版本迭代号
     * 表字段 : CLIENT_VERSION_CODE
     */
    private String clientVersionCode;

    /**
     * 手机MAC地址
     * 表字段 : MAC
     */
    private String mac;

    /**
     * 媒体文件创建时间
     * 表字段 : FILE_TIME
     */
    private Date fileTime;

    /**
     * 分享时间
     * 表字段 : SHARE_TIME
     */
    private Date shareTime;

    /**
     * 是否删除，0-未删除；1-已删除
     * 表字段 : IS_DELETE
     */
    private Integer isDelete;

    /**
     * 获取
     *
     * @return 
     */
    public Long getShareId() {
        return shareId;
    }

    /**
     * 设置
     *
     * @param shareId the value for TB_SHARE.SHARE_ID, 
     */
    public void setShareId(Long shareId) {
        this.shareId = shareId;
    }

    /**
     * 获取与shareId组成联合主键
     *
     * @return 与shareId组成联合主键
     */
    public String getPostId() {
        return postId;
    }

    /**
     * 设置与shareId组成联合主键
     *
     * @param postId the value for TB_SHARE.POST_ID, 与shareId组成联合主键
     */
    public void setPostId(String postId) {
        this.postId = postId == null ? null : postId.trim();
    }

    /**
     * 获取用户标识
     *
     * @return 用户标识
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户标识
     *
     * @param userId the value for TB_SHARE.USER_ID, 用户标识
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 获取客户标识,通过包名来区分
     *
     * @return 客户标识,通过包名来区分
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 设置客户标识,通过包名来区分
     *
     * @param appId the value for TB_SHARE.APP_ID, 客户标识,通过包名来区分
     */
    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    /**
     * 获取分享类型
     *
     * @return 分享类型
     */
    public String getShareType() {
        return shareType;
    }

    /**
     * 设置分享类型
     *
     * @param shareType the value for TB_SHARE.SHARE_TYPE, 分享类型
     */
    public void setShareType(String shareType) {
        this.shareType = shareType == null ? null : shareType.trim();
    }

    /**
     * 获取分享标题,客户订制
     *
     * @return 分享标题,客户订制
     */
    public String getShareTitle() {
        return shareTitle;
    }

    /**
     * 设置分享标题,客户订制
     *
     * @param shareTitle the value for TB_SHARE.SHARE_TITLE, 分享标题,客户订制
     */
    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle == null ? null : shareTitle.trim();
    }

    /**
     * 获取分享内容
     *
     * @return 分享内容
     */
    public String getShareContent() {
        return shareContent;
    }

    /**
     * 设置分享内容
     *
     * @param shareContent the value for TB_SHARE.SHARE_CONTENT, 分享内容
     */
    public void setShareContent(String shareContent) {
        this.shareContent = shareContent == null ? null : shareContent.trim();
    }

    /**
     * 获取分享地址
     *
     * @return 分享地址
     */
    public String getShareAddress() {
        return shareAddress;
    }

    /**
     * 设置分享地址
     *
     * @param shareAddress the value for TB_SHARE.SHARE_ADDRESS, 分享地址
     */
    public void setShareAddress(String shareAddress) {
        this.shareAddress = shareAddress == null ? null : shareAddress.trim();
    }

    /**
     * 获取浏览数量
     *
     * @return 浏览数量
     */
    public Integer getBrowseCount() {
        return browseCount;
    }

    /**
     * 设置浏览数量
     *
     * @param browseCount the value for TB_SHARE.BROWSE_COUNT, 浏览数量
     */
    public void setBrowseCount(Integer browseCount) {
        this.browseCount = browseCount;
    }

    /**
     * 获取喜欢数量
     *
     * @return 喜欢数量
     */
    public Integer getLikeCount() {
        return likeCount;
    }

    /**
     * 设置喜欢数量
     *
     * @param likeCount the value for TB_SHARE.LIKE_COUNT, 喜欢数量
     */
    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * 获取分享地址
     *
     * @return 分享地址
     */
    public String getShareUrl() {
        return shareUrl;
    }

    /**
     * 设置分享地址
     *
     * @param shareUrl the value for TB_SHARE.SHARE_URL, 分享地址
     */
    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl == null ? null : shareUrl.trim();
    }

    /**
     * 获取分享文件名
     *
     * @return 分享文件名
     */
    public String getFilename() {
        return filename;
    }

    /**
     * 设置分享文件名
     *
     * @param filename the value for TB_SHARE.FILENAME, 分享文件名
     */
    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    /**
     * 获取来源于哪个操作系统
     *
     * @return 来源于哪个操作系统
     */
    public String getClientOs() {
        return clientOs;
    }

    /**
     * 设置来源于哪个操作系统
     *
     * @param clientOs the value for TB_SHARE.CLIENT_OS, 来源于哪个操作系统
     */
    public void setClientOs(String clientOs) {
        this.clientOs = clientOs == null ? null : clientOs.trim();
    }

    /**
     * 获取APP版本号
     *
     * @return APP版本号
     */
    public String getClientVersion() {
        return clientVersion;
    }

    /**
     * 设置APP版本号
     *
     * @param clientVersion the value for TB_SHARE.CLIENT_VERSION, APP版本号
     */
    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion == null ? null : clientVersion.trim();
    }

    /**
     * 获取APP版本迭代号
     *
     * @return APP版本迭代号
     */
    public String getClientVersionCode() {
        return clientVersionCode;
    }

    /**
     * 设置APP版本迭代号
     *
     * @param clientVersionCode the value for TB_SHARE.CLIENT_VERSION_CODE, APP版本迭代号
     */
    public void setClientVersionCode(String clientVersionCode) {
        this.clientVersionCode = clientVersionCode == null ? null : clientVersionCode.trim();
    }

    /**
     * 获取手机MAC地址
     *
     * @return 手机MAC地址
     */
    public String getMac() {
        return mac;
    }

    /**
     * 设置手机MAC地址
     *
     * @param mac the value for TB_SHARE.MAC, 手机MAC地址
     */
    public void setMac(String mac) {
        this.mac = mac == null ? null : mac.trim();
    }

    /**
     * 获取媒体文件创建时间
     *
     * @return 媒体文件创建时间
     */
    public Date getFileTime() {
        return fileTime;
    }

    /**
     * 设置媒体文件创建时间
     *
     * @param fileTime the value for TB_SHARE.FILE_TIME, 媒体文件创建时间
     */
    public void setFileTime(Date fileTime) {
        this.fileTime = fileTime;
    }

    /**
     * 获取分享时间
     *
     * @return 分享时间
     */
    public Date getShareTime() {
        return shareTime;
    }

    /**
     * 设置分享时间
     *
     * @param shareTime the value for TB_SHARE.SHARE_TIME, 分享时间
     */
    public void setShareTime(Date shareTime) {
        this.shareTime = shareTime;
    }

    /**
     * 获取是否删除，0-未删除；1-已删除
     *
     * @return 是否删除，0-未删除；1-已删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除，0-未删除；1-已删除
     *
     * @param isDelete the value for TB_SHARE.IS_DELETE, 是否删除，0-未删除；1-已删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}