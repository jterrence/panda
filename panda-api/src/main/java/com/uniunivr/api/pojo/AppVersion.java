package com.uniunivr.api.pojo;

import java.util.Date;

/**
 * @描述:      @app升级表信息实体类
 * @数据库表：  @TB_APP_VERSION
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-06-16
 */
public class AppVersion {

    /**
     * 
     * 表字段 : _ID
     */
    private Long id;

    /**
     * 版本名称
     * 表字段 : VERSION_NAME
     */
    private String versionName;

    /**
     * 版本迭代号
     * 表字段 : VERSION_CODE
     */
    private Integer versionCode;

    /**
     * 包名
     * 表字段 : APP_ID
     */
    private String appId;

    /**
     * 渠道id
     * 表字段 : CHANNEL_ID
     */
    private Integer channelId;

    /**
     * 渠道名称
     * 表字段 : CHANNEL_NAME
     */
    private String channelName;

    /**
     * app类型,Android,IOS
     * 表字段 : APP_TYPE
     */
    private String appType;

    /**
     * app图标
     * 表字段 : APP_ICON
     */
    private String appIcon;

    /**
     * app显示名称
     * 表字段 : APP_NAME
     */
    private String appName;

    /**
     * 更新内容
     * 表字段 : UPDATE_CONTENT
     */
    private String updateContent;

    /**
     * APP安装包大小
     * 表字段 : FILE_SIZE
     */
    private String fileSize;

    /**
     * APP安装包名称
     * 表字段 : FILE_NAME
     */
    private String fileName;

    /**
     * 是否打开下载链接，0-不公开；1-公开
     * 表字段 : IS_PUBLISH
     */
    private Integer isPublish;

    /**
     * app运行状态，0(禁用)；1(正常)
     * 表字段 : APP_STATU
     */
    private Integer appStatu;

    /**
     * 升级方式，0(强制升级)；1(推荐升级)
     * 表字段 : UPGRADE_MODE
     */
    private Integer upgradeMode;

    /**
     * 创建时间
     * 表字段 : CREATE_TIME
     */
    private Date createTime;

    /**
     * 是否删除，0-未删除；1-已删除
     * 表字段 : IS_DELETE
     */
    private Integer isDelete;

    /**
     * 获取
     *
     * @return 
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置
     *
     * @param id the value for TB_APP_VERSION._ID, 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取版本名称
     *
     * @return 版本名称
     */
    public String getVersionName() {
        return versionName;
    }

    /**
     * 设置版本名称
     *
     * @param versionName the value for TB_APP_VERSION.VERSION_NAME, 版本名称
     */
    public void setVersionName(String versionName) {
        this.versionName = versionName == null ? null : versionName.trim();
    }

    /**
     * 获取版本迭代号
     *
     * @return 版本迭代号
     */
    public Integer getVersionCode() {
        return versionCode;
    }

    /**
     * 设置版本迭代号
     *
     * @param versionCode the value for TB_APP_VERSION.VERSION_CODE, 版本迭代号
     */
    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    /**
     * 获取包名
     *
     * @return 包名
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 设置包名
     *
     * @param appId the value for TB_APP_VERSION.APP_ID, 包名
     */
    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    /**
     * 获取渠道id
     *
     * @return 渠道id
     */
    public Integer getChannelId() {
        return channelId;
    }

    /**
     * 设置渠道id
     *
     * @param channelId the value for TB_APP_VERSION.CHANNEL_ID, 渠道id
     */
    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    /**
     * 获取渠道名称
     *
     * @return 渠道名称
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * 设置渠道名称
     *
     * @param channelName the value for TB_APP_VERSION.CHANNEL_NAME, 渠道名称
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName == null ? null : channelName.trim();
    }

    /**
     * 获取app类型,Android,IOS
     *
     * @return app类型,Android,IOS
     */
    public String getAppType() {
        return appType;
    }

    /**
     * 设置app类型,Android,IOS
     *
     * @param appType the value for TB_APP_VERSION.APP_TYPE, app类型,Android,IOS
     */
    public void setAppType(String appType) {
        this.appType = appType == null ? null : appType.trim();
    }

    /**
     * 获取app图标
     *
     * @return app图标
     */
    public String getAppIcon() {
        return appIcon;
    }

    /**
     * 设置app图标
     *
     * @param appIcon the value for TB_APP_VERSION.APP_ICON, app图标
     */
    public void setAppIcon(String appIcon) {
        this.appIcon = appIcon == null ? null : appIcon.trim();
    }

    /**
     * 获取app显示名称
     *
     * @return app显示名称
     */
    public String getAppName() {
        return appName;
    }

    /**
     * 设置app显示名称
     *
     * @param appName the value for TB_APP_VERSION.APP_NAME, app显示名称
     */
    public void setAppName(String appName) {
        this.appName = appName == null ? null : appName.trim();
    }

    /**
     * 获取更新内容
     *
     * @return 更新内容
     */
    public String getUpdateContent() {
        return updateContent;
    }

    /**
     * 设置更新内容
     *
     * @param updateContent the value for TB_APP_VERSION.UPDATE_CONTENT, 更新内容
     */
    public void setUpdateContent(String updateContent) {
        this.updateContent = updateContent == null ? null : updateContent.trim();
    }

    /**
     * 获取APP安装包大小
     *
     * @return APP安装包大小
     */
    public String getFileSize() {
        return fileSize;
    }

    /**
     * 设置APP安装包大小
     *
     * @param fileSize the value for TB_APP_VERSION.FILE_SIZE, APP安装包大小
     */
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize == null ? null : fileSize.trim();
    }

    /**
     * 获取APP安装包名称
     *
     * @return APP安装包名称
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置APP安装包名称
     *
     * @param fileName the value for TB_APP_VERSION.FILE_NAME, APP安装包名称
     */
    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    /**
     * 获取是否打开下载链接，0-不公开；1-公开
     *
     * @return 是否打开下载链接，0-不公开；1-公开
     */
    public Integer getIsPublish() {
        return isPublish;
    }

    /**
     * 设置是否打开下载链接，0-不公开；1-公开
     *
     * @param isPublish the value for TB_APP_VERSION.IS_PUBLISH, 是否打开下载链接，0-不公开；1-公开
     */
    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }

    /**
     * 获取app运行状态，0(禁用)；1(正常)
     *
     * @return app运行状态，0(禁用)；1(正常)
     */
    public Integer getAppStatu() {
        return appStatu;
    }

    /**
     * 设置app运行状态，0(禁用)；1(正常)
     *
     * @param appStatu the value for TB_APP_VERSION.APP_STATU, app运行状态，0(禁用)；1(正常)
     */
    public void setAppStatu(Integer appStatu) {
        this.appStatu = appStatu;
    }

    /**
     * 获取升级方式，0(强制升级)；1(推荐升级)
     *
     * @return 升级方式，0(强制升级)；1(推荐升级)
     */
    public Integer getUpgradeMode() {
        return upgradeMode;
    }

    /**
     * 设置升级方式，0(强制升级)；1(推荐升级)
     *
     * @param upgradeMode the value for TB_APP_VERSION.UPGRADE_MODE, 升级方式，0(强制升级)；1(推荐升级)
     */
    public void setUpgradeMode(Integer upgradeMode) {
        this.upgradeMode = upgradeMode;
    }

    /**
     * 获取创建时间
     *
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime the value for TB_APP_VERSION.CREATE_TIME, 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取是否删除，0-未删除；1-已删除
     *
     * @return 是否删除，0-未删除；1-已删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除，0-未删除；1-已删除
     *
     * @param isDelete the value for TB_APP_VERSION.IS_DELETE, 是否删除，0-未删除；1-已删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}