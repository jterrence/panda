package com.uniunivr.api.pojo;

import java.util.Date;

/**
 * @描述:      @序列号表信息实体类
 * @数据库表：  @TB_SERIAL_NUMBER
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-03-21
 */
public class SerialNumber {

    /**
     * 
     * 表字段 : _ID
     */
    private Long id;

    /**
     * 序列号
     * 表字段 : SERIAL_NUMBER
     */
    private String serialNumber;

    /**
     * 第几次下单、标签
     * 表字段 : ORDER_TAG
     */
    private String orderTag;

    /**
     * 是否量产，0-未量产；1-已量产
     * 表字段 : MP_FINISH
     */
    private Byte mpFinish;

    /**
     * 属于哪个客户
     * 表字段 : CUSTOMER_ID
     */
    private Long customerId;

    /**
     * 创建时间
     * 表字段 : CREATE_TIME
     */
    private Date createTime;

    /**
     * 是否删除，0-未删除；1-已删除
     * 表字段 : IS_DELETE
     */
    private Byte isDelete;

    /**
     * 获取
     *
     * @return 
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置
     *
     * @param id the value for TB_SERIAL_NUMBER._ID, 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取序列号
     *
     * @return 序列号
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * 设置序列号
     *
     * @param serialNumber the value for TB_SERIAL_NUMBER.SERIAL_NUMBER, 序列号
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber == null ? null : serialNumber.trim();
    }

    /**
     * 获取第几次下单、标签
     *
     * @return 第几次下单、标签
     */
    public String getOrderTag() {
        return orderTag;
    }

    /**
     * 设置第几次下单、标签
     *
     * @param orderTag the value for TB_SERIAL_NUMBER.ORDER_TAG, 第几次下单、标签
     */
    public void setOrderTag(String orderTag) {
        this.orderTag = orderTag == null ? null : orderTag.trim();
    }

    /**
     * 获取是否量产，0-未量产；1-已量产
     *
     * @return 是否量产，0-未量产；1-已量产
     */
    public Byte getMpFinish() {
        return mpFinish;
    }

    /**
     * 设置是否量产，0-未量产；1-已量产
     *
     * @param mpFinish the value for TB_SERIAL_NUMBER.MP_FINISH, 是否量产，0-未量产；1-已量产
     */
    public void setMpFinish(Byte mpFinish) {
        this.mpFinish = mpFinish;
    }

    /**
     * 获取属于哪个客户
     *
     * @return 属于哪个客户
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * 设置属于哪个客户
     *
     * @param customerId the value for TB_SERIAL_NUMBER.CUSTOMER_ID, 属于哪个客户
     */
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    /**
     * 获取创建时间
     *
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime the value for TB_SERIAL_NUMBER.CREATE_TIME, 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取是否删除，0-未删除；1-已删除
     *
     * @return 是否删除，0-未删除；1-已删除
     */
    public Byte getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除，0-未删除；1-已删除
     *
     * @param isDelete the value for TB_SERIAL_NUMBER.IS_DELETE, 是否删除，0-未删除；1-已删除
     */
    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }
}