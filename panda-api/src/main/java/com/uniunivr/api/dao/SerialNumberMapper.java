package com.uniunivr.api.dao;

import com.uniunivr.api.pojo.SerialNumber;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @描述：     @序列号操作dao
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Repository
public interface SerialNumberMapper {
    /**
     * 根据主键删除数据库的记录,TB_SERIAL_NUMBER
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录,TB_SERIAL_NUMBER
     *
     * @param record
     */
    int insert(SerialNumber record);

    /**
     * 动态字段,写入数据库记录,TB_SERIAL_NUMBER
     *
     * @param record
     */
    int insertSelective(SerialNumber record);

    /**
     * 根据指定主键获取一条数据库记录,TB_SERIAL_NUMBER
     *
     * @param id
     */
    SerialNumber selectByPrimaryKey(Long id);


    /**
     * 根据序列号获取记录,TB_SERIAL_NUMBER
     */
    SerialNumber selectBySerialNumber(Long customerId,String number);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_SERIAL_NUMBER
     *
     * @param record
     */
    int updateByPrimaryKeySelective(SerialNumber record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_SERIAL_NUMBER
     *
     * @param record
     */
    int updateByPrimaryKey(SerialNumber record);

    /**
     * 获取客户序列号
     * @param customerId
     * @return
     */
    List<String> selectSerialNumberByCustomerId(Long customerId);
}