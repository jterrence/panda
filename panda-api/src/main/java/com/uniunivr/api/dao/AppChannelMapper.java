package com.uniunivr.api.dao;

import com.uniunivr.api.pojo.AppChannel;
import com.uniunivr.common.generic.GenericDao;
import org.springframework.stereotype.Repository;

/**
 * @描述：     @app渠道信息表
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Repository
public interface AppChannelMapper extends GenericDao<AppChannel,Long>{
    /**
     * 根据主键删除数据库的记录,TB_CHANNEL
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录,TB_CHANNEL
     *
     * @param record
     */
    int insert(AppChannel record);

    /**
     * 动态字段,写入数据库记录,TB_CHANNEL
     *
     * @param record
     */
    int insertSelective(AppChannel record);

    /**
     * 根据指定主键获取一条数据库记录,TB_CHANNEL
     *
     * @param id
     */
    AppChannel selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_CHANNEL
     *
     * @param record
     */
    int updateByPrimaryKeySelective(AppChannel record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_CHANNEL
     *
     * @param record
     */
    int updateByPrimaryKey(AppChannel record);


    /**
     * 查询渠道信息
     * @param channelId
     * @return
     */
    AppChannel selectAppChannelByChannId(int channelId);
}