package com.uniunivr.api.dao;

import com.uniunivr.api.pojo.OrderConfig;
import org.springframework.stereotype.Repository;

/**
 * @描述：     @订单配置dao
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Repository
public interface OrderConfigMapper {
    /**
     * 根据主键删除数据库的记录,TB_ORDER_CONFIG
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录,TB_ORDER_CONFIG
     *
     * @param record
     */
    int insert(OrderConfig record);

    /**
     * 动态字段,写入数据库记录,TB_ORDER_CONFIG
     *
     * @param record
     */
    int insertSelective(OrderConfig record);

    /**
     * 根据指定主键获取一条数据库记录,TB_ORDER_CONFIG
     *
     * @param id
     */
    OrderConfig selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_ORDER_CONFIG
     *
     * @param record
     */
    int updateByPrimaryKeySelective(OrderConfig record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_ORDER_CONFIG
     *
     * @param record
     */
    int updateByPrimaryKey(OrderConfig record);
}