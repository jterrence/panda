package com.uniunivr.api.dao;

import com.uniunivr.api.pojo.AppVersion;
import com.uniunivr.common.generic.GenericDao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @描述：     @app升级dao
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Repository
public interface AppVersionMapper extends GenericDao<AppVersion,Long>{
    /**
     * 根据主键删除数据库的记录,TB_APP_VERSION
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录,TB_APP_VERSION
     *
     * @param record
     */
    int insert(AppVersion record);

    /**
     * 动态字段,写入数据库记录,TB_APP_VERSION
     *
     * @param record
     */
    int insertSelective(AppVersion record);

    /**
     * 根据指定主键获取一条数据库记录,TB_APP_VERSION
     *
     * @param id
     */
    AppVersion selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_APP_VERSION
     *
     * @param record
     */
    int updateByPrimaryKeySelective(AppVersion record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_APP_VERSION
     *
     * @param record
     */
    int updateByPrimaryKey(AppVersion record);

    /**
     * 根据包名和渠道id来查询
     * @param appId
     * @param channelId
     * @return
     */
    AppVersion selectByAppIdAndChannelId(@Param("appId") String appId,
                                         @Param("channelId") int channelId);

    /**
     * 查询app运行状态
     * @param appId
     * @return
     */
    int selectAppStatu(@Param("appId") String appId);
}