package com.uniunivr.api.dao;

import com.uniunivr.api.pojo.Customer;
import org.springframework.stereotype.Repository;

/**
 * @描述：     @客户操作dao
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Repository
public interface CustomerMapper {
    /**
     * 根据主键删除数据库的记录,TB_CUSTOMER
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录,TB_CUSTOMER
     *
     * @param record
     */
    int insert(Customer record);

    /**
     * 动态字段,写入数据库记录,TB_CUSTOMER
     *
     * @param record
     */
    int insertSelective(Customer record);

    /**
     * 根据指定主键获取一条数据库记录,TB_CUSTOMER
     *
     * @param id
     */
    Customer selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_CUSTOMER
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Customer record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_CUSTOMER
     *
     * @param record
     */
    int updateByPrimaryKey(Customer record);
}