package com.uniunivr.api.dao;

import com.uniunivr.api.pojo.Share;
import com.uniunivr.common.generic.GenericDao;
import org.springframework.stereotype.Repository;

/**
 * @描述：     @用户分享操作dao
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
@Repository
public interface ShareMapper extends GenericDao<Share,Long>{
    /**
     * 根据主键删除数据库的记录,TB_SHARE
     *
     * @param shareId
     */
    int deleteByPrimaryKey(Long shareId);

    /**
     * 新写入数据库记录,TB_SHARE
     *
     * @param record
     */
    int insert(Share record);

    /**
     * 动态字段,写入数据库记录,TB_SHARE
     *
     * @param record
     */
    int insertSelective(Share record);

    /**
     * 根据指定主键获取一条数据库记录,TB_SHARE
     * 获取分享记录
     * @param shareId
     */
    Share selectByPrimaryKey(Long shareId);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_SHARE
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Share record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_SHARE
     *
     * @param record
     */
    int updateByPrimaryKey(Share record);

    /**
     * 查询最大的主键id
     * @return
     */
    Long selectPrimaryKey();


    /**
     * 插入分享到数据库
     * @param shareInfo
     * @return
     */
    int insertShareInfo(Share shareInfo);

    /**
     * 更新喜欢数量
     * @param shareInfo
     */
    int updateLikeCount(Share shareInfo);

    /**
     * 更新分享内容
     * @param shareInfo
     * @return
     */
    int updateBrowseCount(Share shareInfo);

    /**
     * 获取浏览数量
     * @param shareId
     * @return
     */
    int selectBrowseCount(Long shareId);

    /**
     * 获取喜欢数量
     * @param shareId
     * @return
     */
    int selectLikeCount(Long shareId);

    /**
     * 通过文件名查询分享记录
     * @param postId
     * @return
     */
    Share selectByPostId(String postId);
}