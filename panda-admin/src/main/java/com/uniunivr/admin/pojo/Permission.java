package com.uniunivr.admin.pojo;

/**
 * @描述:      @权限表信息实体类
 * @数据库表：  @TB_PERMISSION
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-03-21
 */
public class Permission {

    /**
     * 权限ID
     * 表字段 : ID
     */
    private Long id;

    /**
     * 权限名
     * 表字段 : PERMISSION_NAME
     */
    private String permissionName;

    /**
     * 权限标识,程序中判断使用,如"user:create"
     * 表字段 : PERMISSION_SIGN
     */
    private String permissionSign;

    /**
     * 权限描述,UI界面显示使用
     * 表字段 : DESCRIPTION
     */
    private String description;

    /**
     * 获取权限ID
     *
     * @return 权限ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置权限ID
     *
     * @param id the value for TB_PERMISSION.ID, 权限ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取权限名
     *
     * @return 权限名
     */
    public String getPermissionName() {
        return permissionName;
    }

    /**
     * 设置权限名
     *
     * @param permissionName the value for TB_PERMISSION.PERMISSION_NAME, 权限名
     */
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName == null ? null : permissionName.trim();
    }

    /**
     * 获取权限标识,程序中判断使用,如"user:create"
     *
     * @return 权限标识,程序中判断使用,如"user:create"
     */
    public String getPermissionSign() {
        return permissionSign;
    }

    /**
     * 设置权限标识,程序中判断使用,如"user:create"
     *
     * @param permissionSign the value for TB_PERMISSION.PERMISSION_SIGN, 权限标识,程序中判断使用,如"user:create"
     */
    public void setPermissionSign(String permissionSign) {
        this.permissionSign = permissionSign == null ? null : permissionSign.trim();
    }

    /**
     * 获取权限描述,UI界面显示使用
     *
     * @return 权限描述,UI界面显示使用
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置权限描述,UI界面显示使用
     *
     * @param description the value for TB_PERMISSION.DESCRIPTION, 权限描述,UI界面显示使用
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}