package com.uniunivr.admin.pojo;

import java.util.Date;

/**
 * @描述:      @用户表信息实体类
 * @数据库表：  @TB_ACCOUNT
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-03-21
 */
public class Account {

    /**
     * 用户ID
     * 表字段 : ID
     */
    private Long id;

    /**
     * 用户名
     * 表字段 : USERNAME
     */
    private String username;

    /**
     * 密码
     * 表字段 : PASSWORD
     */
    private String password;

    /**
     * 状态
     * 表字段 : STATE
     */
    private String state;

    /**
     * 创建时间
     * 表字段 : CREATE_TIME
     */
    private Date createTime;

    /**
     * 获取用户ID
     *
     * @return 用户ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置用户ID
     *
     * @param id the value for TB_ACCOUNT.ID, 用户ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取用户名
     *
     * @return 用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置用户名
     *
     * @param username the value for TB_ACCOUNT.USERNAME, 用户名
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * 获取密码
     *
     * @return 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password the value for TB_ACCOUNT.PASSWORD, 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取状态
     *
     * @return 状态
     */
    public String getState() {
        return state;
    }

    /**
     * 设置状态
     *
     * @param state the value for TB_ACCOUNT.STATE, 状态
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取创建时间
     *
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime the value for TB_ACCOUNT.CREATE_TIME, 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}