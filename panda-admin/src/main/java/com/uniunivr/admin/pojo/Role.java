package com.uniunivr.admin.pojo;

/**
 * @描述:      @角色表信息实体类
 * @数据库表：  @TB_ROLE
 * @作者:      @蒋诗朋
 * @创建时间:   @2017-03-21
 */
public class Role {

    /**
     * 角色ID
     * 表字段 : ID
     */
    private Long id;

    /**
     * 角色名
     * 表字段 : ROLE_NAME
     */
    private String roleName;

    /**
     * 角色标识,程序中判断使用,如"admin"
     * 表字段 : ROLE_SIGN
     */
    private String roleSign;

    /**
     * 角色描述,UI界面显示使用
     * 表字段 : DESCRIPTION
     */
    private String description;

    /**
     * 获取角色ID
     *
     * @return 角色ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置角色ID
     *
     * @param id the value for TB_ROLE.ID, 角色ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取角色名
     *
     * @return 角色名
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置角色名
     *
     * @param roleName the value for TB_ROLE.ROLE_NAME, 角色名
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    /**
     * 获取角色标识,程序中判断使用,如"admin"
     *
     * @return 角色标识,程序中判断使用,如"admin"
     */
    public String getRoleSign() {
        return roleSign;
    }

    /**
     * 设置角色标识,程序中判断使用,如"admin"
     *
     * @param roleSign the value for TB_ROLE.ROLE_SIGN, 角色标识,程序中判断使用,如"admin"
     */
    public void setRoleSign(String roleSign) {
        this.roleSign = roleSign == null ? null : roleSign.trim();
    }

    /**
     * 获取角色描述,UI界面显示使用
     *
     * @return 角色描述,UI界面显示使用
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置角色描述,UI界面显示使用
     *
     * @param description the value for TB_ROLE.DESCRIPTION, 角色描述,UI界面显示使用
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}