package com.uniunivr.admin.dao;

import com.uniunivr.admin.pojo.Permission;

public interface PermissionMapper {
    /**
     * 根据主键删除数据库的记录,TB_PERMISSION
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录,TB_PERMISSION
     *
     * @param record
     */
    int insert(Permission record);

    /**
     * 动态字段,写入数据库记录,TB_PERMISSION
     *
     * @param record
     */
    int insertSelective(Permission record);

    /**
     * 根据指定主键获取一条数据库记录,TB_PERMISSION
     *
     * @param id
     */
    Permission selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_PERMISSION
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Permission record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_PERMISSION
     *
     * @param record
     */
    int updateByPrimaryKey(Permission record);
}