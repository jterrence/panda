package com.uniunivr.admin.dao;

import com.uniunivr.admin.pojo.Account;
import com.uniunivr.common.generic.GenericDao;

public interface AccountMapper extends GenericDao<Account,Long>{
    /**
     * 根据主键删除数据库的记录,TB_ACCOUNT
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新写入数据库记录,TB_ACCOUNT
     *
     * @param record
     */
    int insert(Account record);

    /**
     * 动态字段,写入数据库记录,TB_ACCOUNT
     *
     * @param record
     */
    int insertSelective(Account record);

    /**
     * 根据指定主键获取一条数据库记录,TB_ACCOUNT
     *
     * @param id
     */
    Account selectByPrimaryKey(Long id);

    /**
     * 动态字段,根据主键来更新符合条件的数据库记录,TB_ACCOUNT
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Account record);

    /**
     * 根据主键来更新符合条件的数据库记录,TB_ACCOUNT
     *
     * @param record
     */
    int updateByPrimaryKey(Account record);
}