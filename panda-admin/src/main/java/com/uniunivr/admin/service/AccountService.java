package com.uniunivr.admin.service;

import com.uniunivr.admin.pojo.Account;
import com.uniunivr.common.generic.GenericDao;
import com.uniunivr.common.generic.GenericService;

/**
 * @描述：     @账号业务接口
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public interface AccountService extends GenericService<Account,Long>{

    /**
     * 账户验证
     *
     * @param account
     * @return
     */
    Account authentication(Account account);

    /**
     * 根据用户名查询账户
     *
     * @param accountName
     * @return
     */
    Account selectByAccountName(String accountName);


    /**
     * 查询用户
     * @return
     */
    Account selectByAccount();
}
