package com.uniunivr.admin.service.impl;

import com.uniunivr.admin.dao.AccountMapper;
import com.uniunivr.admin.pojo.Account;
import com.uniunivr.admin.service.AccountService;
import com.uniunivr.common.generic.GenericDao;
import com.uniunivr.common.generic.GenericService;
import com.uniunivr.common.generic.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账户Service实现类
 * @author StarZou
 * @since 2014年7月5日 上午11:54:24
 */
@Service
public class AccountServiceImpl extends GenericServiceImpl<Account,Long> implements AccountService{

    @Resource
    private AccountMapper mAccountMapper;


    @Override
    public Account authentication(Account user) {
//        return mAccountMapper.authentication(user);
        return null;
    }

    @Override
    public GenericDao<Account, Long> getDao() {
        return mAccountMapper;
    }

    @Override
    public Account selectByAccountName(String accountName) {
//           return mAccountMapper.selectByAccountName(accountName);
        return null;
    }

    @Override
    public int insert(Account account) {
        return super.insert(account);
    }

    @Override
    public int update(Account account) {
        return super.update(account);
    }

    @Override
    public int delete(Long id) {
        return super.delete(id);
    }

    @Override
    public Account getById(Long id) {
        return super.getById(id);
    }

    @Override
    public Account getOne() {
        return super.getOne();
    }


    @Override
    public Account selectByAccount() {
        return null;
    }
}
