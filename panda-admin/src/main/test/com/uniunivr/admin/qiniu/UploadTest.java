package com.uniunivr.admin.qiniu;


import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;

import java.io.IOException;

/**
 * @描述：     @七牛SDK测试
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class UploadTest {

    public static final String ACCESS_KEY  = "Vb9rYsx1ueDl6WpaqeK9SsHpRJKng5W86GEvloRY";

    public static final String SECRET_KEY  = "dNnVMXyeFev0iIeVmGeaqH3HPy92egIIesJnPaC4";

    //要上传的空间
    public static final String bucketname  = "lantek";
    //上传到七牛后保存的文件名
    public static final String key         = "15.jpeg";

    public static final String key2        = "common-services.jpg";

    //上传文件的路径
    public static final String FilePath    = "/Users/terrence/Documents/workplace/javaee/panda/panda-share-v2/images/15.jpg";

    public static final String FilePath1   = "/Users/terrence/Documents/workplace/javaee/panda/panda-share-v2/images/common-services.jpg";

    private Auth mAuth;

    private UploadManager mUploadManager;

    public UploadTest(){
        mAuth  = Auth.create(ACCESS_KEY, SECRET_KEY);
        Zone z = Zone.autoZone();
        Configuration c = new Configuration(z);
        //创建上传对象
        mUploadManager = new UploadManager(c);
    }
    

    public static void main(String[] args){

        try {
            new UploadTest().upload();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //简单上传，使用默认策略，只需要设置上传的空间名就可以了
    public String getUpToken() {
        final String token = mAuth.uploadToken(bucketname);
        System.out.print("token--->" + token);
        return token;
    }

    public void upload() throws IOException {
        try {
            final String token = getUpToken();
            //调用put方法上传
            Response res1 = mUploadManager.put(FilePath, key,token);
            //打印返回的信息
            System.out.println(res1.bodyString());

            Response res2 = mUploadManager.put(FilePath1, key2,token);

            //打印返回的信息
            System.out.println(res2.bodyString());
        } catch (QiniuException e) {
            Response r = e.response;
            // 请求失败时打印的异常的信息
            System.out.println(r.toString());
            try {
                //响应的文本信息
                System.out.println(r.bodyString());
            } catch (QiniuException e1) {
                //ignore
            }
        }

    }
}
