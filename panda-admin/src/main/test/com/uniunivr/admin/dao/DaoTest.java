package com.uniunivr.admin.dao;

import com.uniunivr.admin.pojo.Account;
import com.uniunivr.common.page.PageResult;
import com.uniunivr.common.page.PageSearchable;
import com.uniunivr.common.page.PageUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

public class DaoTest {

    private AccountMapper mAccountMapper;

    @Before
    public void before(){
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.getEnvironment().setActiveProfiles("dev");
        ctx.load(new String[]{"classpath:spring-mybatis.xml" ,
                "classpath:spring.xml", "classpath:spring-mvc.xml"});
        ctx.refresh();
        mAccountMapper = (AccountMapper) ctx.getBean("accountMapper");
    }

    @Test
    public void addAccount(){
        for(int i= 0;i < 100;i++){
            final Account account = new Account();
            account.setUsername("ksksjipeng --> " + i);
            account.setPassword(String.valueOf(i));
            account.setCreateTime(new Date());
            account.setState("正常状态");
            final int count = mAccountMapper.insertSelective(account);
            System.out.println("插入" + account.getUsername() + " 成功 --- > " + count);
        }
    }

    @Test
    public void search(){
        final PageSearchable searchable = new PageSearchable(1,20);
        searchable.setSearchProperty("USERNAME");
        searchable.setSearchValue("ksksjipeng");
        final PageResult<Account> result= PageUtils.getPageResult(searchable,mAccountMapper);
        for(Account account:result.getList()){
            System.out.println("账户名称为--->" + account.getUsername());
        }
    }

    @Test
    public void getAccount(){
        final Account account = mAccountMapper.selectByPrimaryKey((long) 2);
        System.out.println(account);
    }

    @Test
    public void updateAccount(){
        final Account account = new Account();
        account.setId((long) 1);
        account.setUsername("专家");
        mAccountMapper.updateByPrimaryKey(account);
    }

    @Test
    public void deleteAccount(){
        mAccountMapper.deleteByPrimaryKey((long) 1);
    }
}
