package com.uniunivr.common.page;

import com.uniunivr.common.generic.GenericDao;

import java.util.List;

/**
 * @描述：     @page工具类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class PageUtils {

    /**
     * 获取查询分页结果
     * @param searchable
     * @param genericDao
     * @param <T>
     * @return
     */
    public static <T> PageResult<T> getPageResult(PageSearchable searchable, GenericDao genericDao){
        int pageNumber  = searchable.getPageNumber();
        int pageSize    = searchable.getPageSize();
        int totalPage   = 0;
        long totalRow   = 0;
        int preRow      = 0;
        if(pageNumber > 1){
            preRow      = (pageNumber-1)*pageSize;
        }
        searchable.setPreRow(preRow);
        totalRow        = genericDao.getCount(searchable);
        totalPage       = (int) (totalRow / pageSize);
        if (totalRow % pageSize != 0) {
            totalPage++;
        }
        List<T> list    = genericDao.selectPagination(searchable);
        return new PageResult<T>(list, pageNumber, pageSize, totalPage, (int)totalRow);
    }
}
