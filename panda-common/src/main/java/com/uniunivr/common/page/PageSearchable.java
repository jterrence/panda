package com.uniunivr.common.page;

import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @描述：     @page查询参数类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class PageSearchable {
    /**
     * 默认当前页
     */
    private final static int DEFAULT_PAGENUMBER = 1;
    /**
     * 默认当前每页显示记录
     */
    private final static int DEFAULT_PAGESIZE   = 20;
    /**
     * 当前页数
     */
    private int pageNumber;
    /**
     * 每页的记录数
     */
    private int pageSize;
    /**
     * 前几页的记录数
     */
    private int preRow;
    /**
     * 要查询的参数
     */
    private String searchProperty;
    /**
     * 要查询的值
     */
    private String searchValue;
    /**
     * 排序的参数
     */
    private String orderProperty;
    /**
     * 排序的方向
     */
    private String orderDirection;

    public PageSearchable() {

    }

    public PageSearchable(HttpServletRequest request) {
        setValue(request);
    }

    public PageSearchable(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize   = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPreRow() {
        return preRow;
    }

    public void setPreRow(int preRow) {
        this.preRow = preRow;
    }

    public String getSearchProperty() {
        return searchProperty;
    }

    public void setSearchProperty(String searchProperty) {
        this.searchProperty = searchProperty;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getOrderProperty() {
        return orderProperty;
    }

    public void setOrderProperty(String orderProperty) {
        this.orderProperty = orderProperty;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    public void setValue(HttpServletRequest request) {

        final String number = request.getParameter("pageNumber");
        if (!StringUtils.isEmpty(number)) {
            pageNumber      = Integer.parseInt(number);
        } else {
            pageNumber      = DEFAULT_PAGENUMBER;
        }

        final String size   = request.getParameter("pageSize");
        if (!StringUtils.isEmpty(size)) {
            pageSize        = Integer.parseInt(size);
        } else {
            pageSize        = DEFAULT_PAGESIZE;
        }

        final String property= request.getParameter("searchProperty");
        if (!StringUtils.isEmpty(property)) {
            searchProperty  = request.getParameter(property);
        }

        final String value  = request.getParameter("searchValue");
        if (!StringUtils.isEmpty(value)) {
            searchValue     = value;
        }

        final String order  = request.getParameter("orderProperty");
        if (!StringUtils.isEmpty(order)) {
            orderProperty   = order;
        }

        final String direction  = request.getParameter("orderDirection");
        if (!StringUtils.isEmpty(direction)) {
            orderDirection  = direction;
        }
    }

}
