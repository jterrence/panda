package com.uniunivr.common.page;

import java.io.Serializable;
import java.util.List;

/**
 * @描述：     @page实体返回结果集
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class PageResult<T> implements Serializable{
    /**
     * 保存查询的数据
     */
    private List<T> list;
    /**
     * 当前页
     */
    private int pageNumber;
    /**
     * 每页的数据数量
     */
    private int pageSize;
    /**
     * 总页数
     */
    private int totalPage;
    /**
     * 总记录数
     */
    private int totalRow;

    public PageResult(List<T> list, int pageNumber,
                      int pageSize, int totalPage, int totalRow) {
        super();
        this.list       = list;
        this.pageNumber = pageNumber;
        this.pageSize   = pageSize;
        this.totalPage  = totalPage;
        this.totalRow   = totalRow;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
