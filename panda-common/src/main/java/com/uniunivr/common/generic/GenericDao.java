package com.uniunivr.common.generic;

import com.uniunivr.common.page.PageSearchable;
import com.uniunivr.common.page.PageResult;

import java.util.List;

/**
 * @描述：     @所有自定义Dao的顶级接口, 封装常用的增删查改操作,
 *            @可以通过Mybatis Generator Maven 插件自动生成Dao,
 *            @也可以手动编码,然后继承GenericDao 即可.
 *            @T:代表数据库中的表 映射的Java对象类型
 *            @PK:代表对象的主键类型
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public interface GenericDao<T, PK> {

    /**
     * 插入对象
     *
     * @param T 对象
     */
    int insertSelective(T T);

    /**
     * 更新对象
     *
     * @param T 对象
     */
    int updateByPrimaryKeySelective(T T);

    /**
     * 通过主键, 删除对象
     *
     * @param id 主键
     */
    int deleteByPrimaryKey(PK id);

    /**
     * 通过主键, 查询对象
     *
     * @param id 主键
     * @return
     */
    T selectByPrimaryKey(PK id);

    /**
     * 按条件分页查询
     * @param searchable
     * @return
     */
    List<T> selectPagination(PageSearchable searchable);

    /**
     * 获取获取条件查询的记录总数
     * @param pageable
     * @return
     */
    Long getCount(PageSearchable pageable);

}
