package com.uniunivr.common.generic;

import java.util.List;

/**
 * 所有自定义Service的顶级接口,封装常用的增删查改操作
 * <p/>
 * T : 代表数据库中的表 映射的Java对象类型
 * PK :代表对象的主键类型
 * @描述：     @用户分享操作dao
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public interface GenericService<T, PK> {

    /**
     * 插入对象
     *
     * @param t 对象
     */
    int insert(T t);

    /**
     * 更新对象
     *
     * @param t 对象
     */
    int update(T t);

    /**
     * 通过主键, 删除对象
     *
     * @param id 主键
     */
    int delete(PK id);

    /**
     * 通过主键, 查询对象
     * @param id 主键
     * @return T 对象
     */
    T getById(PK id);


    /**
     * 查询单个对象
     *
     * @return 对象
     */
    T getOne();


    /**
     * 查询多个对象
     *
     * @return 对象集合
     */
    List<T> getList();

}
