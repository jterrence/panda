package com.uniunivr.common.controller;

import com.alibaba.fastjson.JSONObject;
import com.uniunivr.common.model.ResultModel;
import com.uniunivr.common.property.CustomizedPropertyConfigurer;
import com.uniunivr.common.util.ResponseUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.*;

/**
 * @描述：     @基灯控制类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */

public class BaseController {

    public static final int TRUE    = 1;

    public static final int FALSE   = 0;

    /**
     * 响应结果码
     */
    private int mResultCode         = ResponseUtils.
            RESPONSE_PANDA_SUCCESS_STATUS_CODE;

    public Logger logger         = (Logger) LogManager.
            getLogger(LogManager.ROOT_LOGGER_NAME);

    public int getResultCode() {
        return this.mResultCode;
    }

    public void setResultCode(int resultCode) {
        this.mResultCode = resultCode;
    }

    @Autowired
    protected HttpServletRequest mHttpRequest;

    /**
     * 获取客户端操作系统
     * @return
     */
    public final String getClientOS(){
        return mHttpRequest.getHeader("clientOS");
    }

    /**
     * 获取客户端操作系统
     * @return
     */
    public final String getClientOS(HttpServletRequest request){
        return request.getHeader("clientOS");
    }

    /**
     * 获取客户端版本号
     * @return
     */
    public final String getClientVersion(){
        return mHttpRequest.getHeader("clientVersion");
    }

    /**
     * 获取客户端版本号
     * @return
     */
    public final String getClientVersion(HttpServletRequest request){
        return request.getHeader("clientVersion");
    }

    /**
     * 获取客户端版本迭代号
     * @return
     */
    public final String getClientVersionCode(){
        return mHttpRequest.getHeader("clientVersionCode");
    }


    /**
     * 获取客户端版本迭代号
     * @return
     */
    public final String getClientVersionCode(HttpServletRequest request){
        return request.getHeader("clientVersionCode");
    }

    /**
     * 获取客户端mac地址
     * @return
     */
    public final String getClientMac(){
        return mHttpRequest.getHeader("MAC");
    }

    /**
     * 获取客户端mac地址
     * @return
     */
    public final String getClientMac(HttpServletRequest request){
        return request.getHeader("MAC");
    }
    /**
     * 获取参数名称
     * @param key
     * @return
     */
    public final Object getParameter(String key){
        return mHttpRequest.getParameter(key);
    }


    /**
     * 获取长整形参数
     * @param key
     * @return
     */
    public final Long getLongParameter(String key){
        final String parameter  = getStringParameter(key);
        return null != parameter?Long.parseLong(parameter):null;
    }

    /**
     * 获取字符串参数
     * @param key
     * @return
     */
    public final String getStringParameter(String key){
        final String parameter  = (String) getParameter(key);
        return parameter;
    }


    /**
     * 获取整形参数
     * @param key
     * @return
     */
    public final Integer getIntegerParameter(String key){
        final String parameter  = getStringParameter(key);
        return null != parameter? Integer.parseInt(parameter):null;
    }

    /**
     * 获取float参数
     * @param key
     * @return
     */
    public final Float getFloatParameter(String key){
        final String parameter  = getStringParameter(key);
        return null != parameter? Float.parseFloat(parameter):null;
    }

    /**
     * 获取double参数
     * @param key
     * @return
     */
    public final Double getDoubleParameter(String key){
        final String parameter  = getStringParameter(key);
        return null != parameter? Double.parseDouble(parameter):null;
    }

    /**
     * 响应客户端数据
     * @param response
     * @param json
     * @throws IOException
     */
    public final void write(HttpServletResponse response,String json) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(json);
    }

    /**
     * 把业务数据转换成json数据
     * @param model
     * @return
     */
    public final String toJSON(Object model){
        return JSONObject.toJSON(model).toString();
    }

    /**
     * 设置响应状态码
     * @param result
     * @param rtn
     */
    public final void obtainResponseMessage(ResultModel result,int rtn){
        result.setRtn(rtn);
        result.setMsg(CustomizedPropertyConfigurer.getProperty(String.valueOf(rtn)));
    }

    /**
     * 异步执行，并返回结果
     * @param response
     * @param callable
     * @param <T>
     * @throws Exception
     */
    public final <T> void execute(HttpServletResponse response,Callable<T> callable)
            throws Exception {
        final ExecutorService service     = Executors.newSingleThreadExecutor();
        final Future<T> future            = service.submit(callable);
        if(null != future){
            ResultModel<T> result         = (ResultModel<T>) future.get();
            if(null != result){
                write(response,toJSON(result));
            }
        }

        if(!service.isShutdown()){
            service.shutdown();
        }
    }

    /**
     * 判断对象是否为空
     * @param object
     */
    public final boolean isEmpty(Object object){
        return StringUtils.isEmpty(object);
    }

}
