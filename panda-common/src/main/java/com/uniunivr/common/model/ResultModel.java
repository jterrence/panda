package com.uniunivr.common.model;

import com.uniunivr.common.util.ResponseUtils;

/**
 * @描述：     @请求响应实体
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class ResultModel<T> {
    /**
     * 返回结果码
     */
    private int      rtn;

    /**
     * 返回消息
     */
    private String   msg;

    private T data;


    public int getRtn() {
        return rtn;
    }

    public void setRtn(int rtn) {
        this.rtn = rtn;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResultModel(){

    }

    public ResultModel(T data){
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
