package com.uniunivr.common.util;

import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * @描述： @Process工具操作类
 * @作者： @蒋诗朋
 * @创建时间： @2013-11-5
 */
public class ProcessBuilderUtils {
    /**
     * 执行命令
     * @param builder
     */
    public static final int executeCommand(ProcessBuilder builder,boolean showlog){
        InputStreamReader ir             = null;
        LineNumberReader input           = null;
        Process process                  = null;
        int result                       = 0;
        try{
            process                      = builder.start();
            ir                           = new InputStreamReader(process
                    .getInputStream());
            input                        = new LineNumberReader(ir);
            String line;
            if(showlog){
                while ((line = input.readLine()) != null) {
                    System.out.println(line);
                }
            }
            result                       = process.waitFor();
        }catch (Exception e){

        }finally {
            if(null != input){
                try{
                    input.close();
                }catch (Exception e){

                }
            }

            if(null != ir){
                try{
                    ir.close();
                }catch (Exception e){

                }
            }

            if(null != process){
                process.destroy();
            }
        }
        return result;
    }
}
