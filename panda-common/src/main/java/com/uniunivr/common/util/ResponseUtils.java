package com.uniunivr.common.util;

/**
 * @描述：     @http响应工具类
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class ResponseUtils {
    /**
     * 响应成功
     */
    public static int RESPONSE_PANDA_SUCCESS_STATUS_CODE     = 1;

    /**
     * 响应失败
     */
    public static int RESPONSE_PANDA_FAILURE_STATUS_CODE     = 0;


    /**
     * 无效的参数
     */
    public static int RESPONSE_PANDA_FAILURE_PARAM_ERROR     = 2;

    /**
     * 数据库操作异常
     */
    public static int RESPONSE_PANDA_FAILURE_DATA_BASE_ERROR = 3;


    /**
     * 上传图片尺寸过大
     */
    public static int RESPONSE_PANDA_IMAGE_MAX_SIZE_LIMIT    = 4;

    /**
     * 上传视频尺寸过大
     */
    public static int RESPONSE_PANDA_VIDEO_MAX_SIZE_LIMIT    = 5;

    /**
     * 上传文件格式出错
     */
    public static int RESPONSE_PANDA_FILE_FORMAT_ERROR       = 6;

    /**
     * http请求成功响应码
     */
    public static int RESPONSE_HTTP_SUCCESS_STATUS_CODE      = 200;


}
