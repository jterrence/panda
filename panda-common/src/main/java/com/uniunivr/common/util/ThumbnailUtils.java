package com.uniunivr.common.util;

import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by terrence on 17-4-6.
 */
public class ThumbnailUtils {

    /**
     * 生成缩略图
     * @param inputPath
     * @param outPath
     * @param thumbWidth
     * @param thumbHeight
     * @throws IOException
     */
    public static final void thumbnail(String inputPath, String outPath,int thumbWidth, int thumbHeight)
            throws IOException {
        Thumbnails.of(inputPath)
                .size(thumbWidth, thumbHeight)
                .toFile(outPath);
    }

}
