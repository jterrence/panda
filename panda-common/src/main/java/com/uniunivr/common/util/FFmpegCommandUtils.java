package com.uniunivr.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @描述： @ffmpeg命令工具操作类
 * @作者： @蒋诗朋
 * @创建时间： @2013-11-5
 */
public class FFmpegCommandUtils {

    /**
     * 从指定的mp4文件当中提取音频
     * @param inputFile
     * @param outputFile
     * @param showlog
     */
    public static void separateAudioFromMp4(String rootDir,
                                            String inputFile,String outputFile,boolean showlog){
        final String[] commands          = new String[]{
                rootDir,
                "-i",
                inputFile,
                "-f",
                "mp3",
                "-vn",
                outputFile
        };
        ProcessBuilder builder           = new ProcessBuilder(commands);
        System.out.println("开始执行提取音频命令----");
        ProcessBuilderUtils.executeCommand(builder,showlog);
        System.out.println("执行提取音频命令完成----");

    }

//    /**
//     * 生成视频截图
//     * @param rootDir
//     * @param inputFile
//     * @param outputFile
//     * @param showlog
//     */
//    public static void screenshot(String rootDir,String inputFile,
//                                  String outputFile,String size,boolean showlog){
//        final String[] commands          = new String[]{
//                rootDir,
//                "-y",
//                "-ss",
//                "2",
//                "-t",
//                "5",
//                "-i",
//                inputFile,
//                "-vf",
//                "select=eq(pict_type\\,I)",
//                "-vframes",
//                "1",
//                "-f",
//                "image2",
//                "-s",
//                size,
//                outputFile
//        };
//        Process process                  = null;
//        ProcessBuilder builder           = new ProcessBuilder(commands);
//        System.out.println("开始执行生成截图命令开始---");
//        ProcessBuilderUtils.executeCommand(builder,showlog);
//        System.out.println("执行生成截图命令完成---");
//    }

    /**
     * 生成视频截图
     * @param rootDir
     * @param inputFile
     * @param outputFile
     * @param showlog
     */
    public static void screenshot(String rootDir,String inputFile,
                                  String outputFile,String size,boolean showlog){
        final String[] commands          = new String[]{
                rootDir,
                "-i",
                inputFile,
                "-y",
                "-f",
                "image2",
                "-t",
                "0.001",
                "-s",
                size,
                outputFile
        };
        ProcessBuilder builder           = new ProcessBuilder(commands);
        System.out.println("开始执行生成截图命令开始---");
        ProcessBuilderUtils.executeCommand(builder,showlog);
        System.out.println("执行生成截图命令完成---");
    }

    /**
     * 转换不同的分辨率
     * @param rootDir
     * @param inputFile
     * @param outputPath
     * @param showlog
     */
    public static final void convertResolution(
            String rootDir,String inputFile,String outputPath,String resolution,boolean showlog){
        final String[] commands          = new String[]{rootDir,"-i",
                inputFile,"-s",resolution,outputPath};
        ProcessBuilder builder           = new ProcessBuilder(commands);
        System.out.println("开始执行转换分辨率命令--------");
        ProcessBuilderUtils.executeCommand(builder,showlog);
        System.out.println("执行转换分辨率命令完成--------");
    }

}
