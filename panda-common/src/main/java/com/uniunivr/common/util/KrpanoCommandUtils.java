package com.uniunivr.common.util;

import com.uniunivr.common.page.PageUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @描述： @krpano命令行操作工具
 * @作者： @蒋诗朋
 * @创建时间： @2013-11-5
 */
public class KrpanoCommandUtils {
    /**
     * 生成cube工具方法
     * @param toolRootDir
     * @param inputFile
     * @param showlog
     */
    public static int makeCubeImage(String toolRootDir,String inputFile,boolean showlog){
        final String[] commands     = new String[]{
                toolRootDir,
                "makepano",
                "-config=templates/vtour-multires_custom.config",
                inputFile
        };
        final ProcessBuilder builder = new ProcessBuilder(commands);
        return ProcessBuilderUtils.executeCommand(builder,showlog);
    }

    /**
     * 生成全景漫游图片页面
     * @param toolRootDir
     * @param inputFile
     * @param options
     * @param showlog
     * @return
     */
    public static final int makeVtourImagePage(
            String toolRootDir, String inputFile, List<String> options, boolean showlog){
        final LinkedList<String> settings    = new LinkedList<>();
        settings.add(toolRootDir);
        settings.add("makepano");
        settings.add("-config=templates/vtour-multires_custom.config");
        settings.addAll(options);
        settings.add(inputFile);
        final String[] commands      = settings.toArray(new String[settings.size()]);
        final ProcessBuilder builder = new ProcessBuilder(commands);
        return ProcessBuilderUtils.executeCommand(builder,showlog);
    }

    /**
     * 通过命令行注册krpanotool
     * @param toolRootDir
     * @param key
     * @return
     */
    public static final int registerKrpanotools(String toolRootDir, String key,boolean showlog){
        final LinkedList<String> settings    = new LinkedList<>();
        settings.add(toolRootDir);
        settings.add("register");
        settings.add(key);
        final String[] commands      = settings.toArray(new String[settings.size()]);
        final ProcessBuilder builder = new ProcessBuilder(commands);
        return ProcessBuilderUtils.executeCommand(builder,showlog);
    }

    /**
     * 生成全景漫游视频页面
     * @param toolRootDir
     * @param inputFile
     * @param options
     * @param showlog
     * @return
     */
    public static final int makeVtourVideoPage(
            String toolRootDir, String inputFile, List<String> options, boolean showlog){
        final LinkedList<String> settings    = new LinkedList<>();
        settings.add(toolRootDir);
        settings.add("makepano");
        settings.add("-config=templates/vtour-video.config");
        settings.addAll(options);
        settings.add(inputFile);
        final String[] commands      = settings.toArray(new String[settings.size()]);
        final ProcessBuilder builder = new ProcessBuilder(commands);
        return ProcessBuilderUtils.executeCommand(builder,showlog);
    }

    /**
     * 组装页面静态信息
     * @param content
     * @param createTime
     * @return
     */
    public static final LinkedList<String> buildVtourPageInfo(String content,String createTime){
        final LinkedList<String> options = new LinkedList<>();
        options.add("-htmltemplate_var=content:" + content);
        options.add("-htmltemplate_var=createTime:" + createTime);
        return options;
    }

    /**
     * 程序拼接好url打包生成
     * @param pathSuffix
     * @param fileSuffix
     * @return
     */
    public static final LinkedList<String> buildVtourCustomOptions(
            String pathSuffix,String prefixJpg,String prefixThumb,String uploadImageDir,String xmlpath,String fileSuffix){
        final LinkedList<String> options = new LinkedList<>();
        options.add("-tilepath=" + uploadImageDir + prefixJpg+fileSuffix+"/[c/]l%Al/%Av/l%Al[_c]_%Av_%Ah.jpg");
        options.add("-tilepathxml=" + pathSuffix + prefixJpg+fileSuffix+"/%s/l1/%v/l1_%s_%v_%h.jpg");
        options.add("-previewpath="+ uploadImageDir + prefixJpg+fileSuffix+"/preview.jpg");
        options.add("-previewpathxml="+ pathSuffix + prefixJpg+fileSuffix+"/preview.jpg");
        options.add("-thumbpath="+ uploadImageDir + prefixThumb+fileSuffix+"/"+fileSuffix+ ".jpg");
        options.add("-thumbpathxml="+ pathSuffix + prefixThumb+fileSuffix+"/"+fileSuffix+ ".jpg");
        options.add("-xmlpath="+ xmlpath + fileSuffix + ".xml");
        return options;
    }
}
