package com.uniunivr.common.listener;

import com.uniunivr.common.config.PandaConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

/**
 * Created by terrence on 17-3-4.
 */
public class WebContextListener extends ContextLoaderListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        String active = context.getInitParameter("spring.profiles.active");
        PandaConfig.setProfileActive(active);
    }
}
