package com.uniunivr.common.config;

/**
 * @描述：     @全局配置
 * @作者：     @蒋诗朋
 * @创建时间： @2016-12-25
 */
public class PandaConfig {

    public static final String SHARE_DOMAIN_PROD = "http://app.uniunivr.com/p/";

    public static final String SHARE_DOMAIN_DEV  = "http://s.uniunivr.com:8080/p/";

    public static final String API_DOMAIN        = "api.uniunivr.com";

    public static final String STATIC_DOMAIN_DEV = "http://s.uniunivr.com:8080/";

    public static final String STATIC_DOMAIN_PROD= "http://app.uniunivr.com/";

    /**
     * 激情profile
     */
    private static String sActive;


    /**
     * 获取分享url
     * @return
     */
    public final static String getShareUrl(){
        if("dev".equals(sActive)){
            return SHARE_DOMAIN_DEV;
        }else{
            return SHARE_DOMAIN_PROD;
        }
    }

    /**
     * 获取静态服务器域名
     * @return
     */
    public final static String getStaticUrl(){
        if("dev".equals(sActive)){
            return STATIC_DOMAIN_DEV;
        }else{
            return STATIC_DOMAIN_PROD;
        }

    }

    /**
     * 保存当前环境变量
     * @param active
     */
    public static final void setProfileActive(String active){
        sActive = active;
    }

    /**
     * 根据环境获取url前缀
     * @return
     */
    public static final String getShareSuffix(){
        if("dev".equals(sActive)){
            return "http://localhost:8080/api";
        }else{
            return "http://api.uniunivr.com";
        }
    }
}
