(function(){
    function nav() {
        $("#unityvc-nav").css({
            top: 0
        })

        var navigation = responsiveNav(".nav-collapse", {
            animate: false,
            transition: 50,
            label: "Menu",
            insert: "after",
            customToggle: "",
            closeOnNavClick: false,
            openPos: "relative",
            navClass: "nav-collapse",
            navActiveClass: "js-nav-active",
            jsClass: "js",
            init: function () {
            },
            open: function () {
                $("#unityvc-nav .nav-collapse").addClass('black');
                $("#unityvc-nav").addClass('c-bg');
                $("#unityvc-nav").css({
                    'height': $(window).height()+'px'
                });
                console.log($(window).height())
            },
            close: function () {
                $("#unityvc-nav .nav-collapse").removeClass('black');
                $("#unityvc-nav").removeClass('c-bg');
                $("#unityvc-nav").css({
                    'height':'auto'
                });
            }
        });

        $(".menuMoble .mobleLi").click(function () {
            navigation.toggle();
        });
    }
    nav();


    function screenHeight() {
        console.log($(window).height())
        var $navHeight = $("#unityvc-nav").height();
        $(".news-warrper").css({
            minHeight: $(window).height() - $navHeight
        });
        $(".news-warrper .menu-left").height($(document).height() - $navHeight)
    }
    screenHeight();


}());