var currentModel = 0,
	isClickMenu  = false,
	islike 		 = false;

$(".stage").height($(window).height() + 8);

var currentIndex= 1;
var nextIndex 	= 1;

//显示模式事件
$(".showmode span").on("touchstart click",function (e) {
    e.stopPropagation();
    e.preventDefault();

	nextIndex ++;
	currentIndex 	= nextIndex;

    var curModelStr = "skin_view_normal();";
    var krpano 		= document.getElementById("krpanoSWFObject");

	switch(nextIndex){
    	//旋转以后模式(鱼眼)
		case 1:
            curModelStr = "skin_view_normal();";
            $("#mode").text('普通');
			break;
		//普通
        case 2:
            curModelStr = "skin_view_fisheye();";
            $("#mode").text('鱼眼');
            break;
		//小行星
        case 3:
            curModelStr = "skin_view_littleplanet();";
            $("#mode").text('小行星');
            break;
		//水晶球
        case 4:
            curModelStr = "skin_view_crystal_ball();";
            $("#mode").text('水晶球');
            break;
		//vr
        case 5:
            curModelStr = "skin_view_vr();";
            $("#mode").text('vr');
            currentIndex= 0;
            nextIndex   = 0;
            togglelikeInfo(true);
            break;
	}

    krpano.call(curModelStr);
});

$(".morelist li").live("touchstart click", function(e) {
	$(this).addClass("active").siblings().removeClass("active");
	if ($(this).index() == 2) {
		e.stopPropagation();
		e.preventDefault();
		$(".mask").show();
		$(".more").removeClass("active");
		$(".personInfo").removeClass("active");
		// $(".publicAccount").addClass("active");
	} else if ($(this).index() == 4) {
		e.stopPropagation();
		e.preventDefault();
		$(".mask").show();
		$(".more").removeClass("active");
		$(".publicAccount").removeClass("active");
		$(".personInfo").addClass("active");
	} else if ($(this).index() == 5) {
		if (isiOS && isWeiXin()) { //
			e.stopPropagation();
			e.preventDefault();
			$(".msg").addClass('active');
			setTimeout(function() {
				$(".msg").removeClass('active');
			}, 10000);
		}
		setTimeout(function() {
			$(".mask").hide();
			$(".more").removeClass("active");
		}, 500);
	} else {
		setTimeout(function() {
			$(".mask").hide();
			$(".more").removeClass("active");
		}, 500);
	}
});

$("#pano").on('touchstart click', function(e) {
	e.stopPropagation();
	e.preventDefault();
	vrclick();
})

$(".exitvr").on('touchstart click', function(e) {
	e.stopPropagation();
	e.preventDefault();
	var krpano = document.getElementById("krpanoSWFObject");
	$(this).removeClass("active");
	krpano.call('webvr.exitVR();');
})

$(".close").on('touchstart click', function(e) {
	e.stopPropagation();
	e.preventDefault();
	$(".bottomInfo,.placeholder").addClass('outInfo');
	setTimeout(function(){
		$(".stage .right").addClass('adjust');
	},60);
});

$(".userlike .like").on('touchstart click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    if (!islike) {
        //更新喜欢数量
        updateLikeCount($(this));
        islike = true;
    }
});



function togglelikeInfo(isShow){
	if(isShow=="true"){
		$(".userlike-showmode").hide();
		$(".share-info").hide();
	}else{
		$(".userlike-showmode").show();
        $(".share-info").show();
	}
}

function ShowMore() {
	$(".mask").show();
	$(".more").toggleClass("active");
}

function DisplayMain(isShow) {
	if (isShow == 'true')
		$(".main").show();
	else {
		$(".main").hide();
	}
}

function vrclick() {
	var krpano = document.getElementById("krpanoSWFObject");
	krpano.call('set_vr_dis();');
}

function exitShow() {
	var el = $(".exitvr");
	el.addClass("active");
	setTimeout(function() {
		el.removeClass("active");
	}, 3500);
}

/**
 * 退回vr模式,返回到鱼眼模式
 */
function exitVR() {
    var krpano 		= document.getElementById("krpanoSWFObject");
    setTimeout(function() {
        krpano.call("skin_view_normal();");
        $("#mode").text('普通');
        currentIndex= 1;
        nextIndex 	= 1;
    }, 600);
}

function getMenuStatus() {
	return isClickMenu;
}

// ########### 提供接口，给krpano的配置文件获取参数  ###########
// 获取视频地址
function getVideoPath() {
	var videoPath = "../video/" + filename;

	// 如果带音频则加入，格式为 xxx.mp4|xxx.m4a
	if (videoVoice != null) {
		var voicePath = "../video/" + videoVoice;
		videoPath = videoPath + "|" + voicePath;
	}
	//alert("getVideoPath, return : " + videoPath);
	return videoPath;
}

// 获取视频截图地址
function getVideoPosterPath() {
	var videoPosterPath = "",
		tempFileName = "";
	if (videoShot != null) {
		videoPosterPath = "../video/" + videoShot;
	}
	return videoPosterPath;
}

function isWeiXin() {
	var ua = window.navigator.userAgent.toLowerCase();
	if (ua.match(/MicroMessenger/i) == 'micromessenger') {
		return true;
	} else {
		return false;
	}
}

(function() {
	var preorientation = null;
	var supportOrientation = (typeof window.orientation == "number" && typeof window.onorientationchange == "object");
	var updateOrientation = function() {
		if (supportOrientation) {
			updateOrientation = function() {
				var orientation = window.orientation;
				switch (orientation) {
					case 90:
					case -90:
						orientation = "landscape";
						break;
					default:
						orientation = "portrait";
				}

				var krpano = document.getElementById("krpanoSWFObject");
				krpano.call("updateorientation(" + orientation + ")");
			};
		} else {
			updateOrientation = function() {
				var orientation = (window.innerWidth > window.innerHeight) ? "landscape" : "portrait";
				if (preorientation == orientation) return;
				var krpano = document.getElementById("krpanoSWFObject");
				krpano.call("updateorientation(" + orientation + ")");
				preorientation = orientation;
			};
		}
		updateOrientation();
	};

	var init = function() {
		if (u.toLowerCase().indexOf('mqqbrowser') && isAndroid) {
			supportOrientation = false;
		} else if (isWeiXin() && isAndroid) {
			supportOrientation = false;
		}
		if (supportOrientation) {
			window.addEventListener("orientationchange", updateOrientation, false);
		} else {
			window.setInterval(updateOrientation, 500);
		}
		if(u.toLowerCase().indexOf('mqqbrowser') > 0||u.toLowerCase().indexOf('ucbrowser') > 0){
//			$(".stage").height($(window).height() - $(".bottomInfo").height() + 8);
		}
	};
	window.addEventListener("DOMContentLoaded", init, false);
	$(".placeholder img").bind('load',function(){
//		$(".stage").height($(window).height() - $(".bottomInfo").height() + 8);
	})
})();