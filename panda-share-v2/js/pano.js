var u = navigator.userAgent;
var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
// 获得url的参数值
function GetQueryString(name) {
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    //if(r!=null)return  unescape(r[2]); return null;  //
    if (r != null) return decodeURI(r[2]); return null;
}

//分享id
var shareId      = GetQueryString("shareId");

var content      = GetQueryString("content");

//测试环境
var devSuffix    = "http://localhost:8080/api/share";
//生产环境
var prodSuffix   = "http://api.uniunivr.com/share";

var suffix 	     = prodSuffix;

var fileName     = null;

shareId          = 11;

if(shareId != null){
    if(content != null){
        document.title = content + "-UU360全景分享";
        $("#shareContent").text(content);
    }

    alert("shareId -->" + shareId)
    //获取分享信息
    getShareInfo();
}

/**
 * 格式化日期
 * @param format
 * @returns {*}
 */
Date.prototype.format =function(format) {
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(), //day
        "h+" : this.getHours(), //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter
        "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
        (this.getFullYear()+"").substr(4- RegExp.$1.length));
    for(var k in o)if(new RegExp("("+ k +")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length==1? o[k] :
                ("00"+ o[k]).substr((""+ o[k]).length));
    return format;
}

//获取分享信息
function getShareInfo() {
    $.ajax({
        type : "POST",  //提交方式
        url : suffix + "/getShareInfo",//路径
        data : {
            "shareId":shareId
        },//数据，这里使用的是Json格式进行传输
        dataType:'json',
        success : function(result) {//返回数据根据结果进行相应的处理

            fileName = result.fileName;

            // fileName = "../images/15.jpg";

            fileName    ="http://image.uniunivr.com:8080/15.jpg";

            //组件类型
            loadPano(result.shareType);
            //浏览数量
            $("#viewcount").html(result.browseCount + 1 + "&nbsp;人浏览过");
            //喜欢数量
            $("#likecount").text(result.likeCount);

            //分享内容
            if(result.content != null){
                document.title = result.content + "-UU360全景分享";
                $("#shareContent").text(result.content);
            }

            //分享时间
            if(result.createTime != null){
                var createTime = new Date(result.createTime).format("yyyy-MM-dd")
                $("#shareDate").html("发布时间:&nbsp;&nbsp;" + createTime);
            }

            updateShareInfo();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus)
        }
    });
}

//更新浏览数量
function updateShareInfo() {
    $.ajax({
        type : "POST",  //提交方式
        url : suffix +"/updateShareContent",//路径
        data : {
            "shareId":shareId,
            "content":content
        },//数据，这里使用的是Json格式进行传输
        dataType:'json',
        success : function(result) {//返回数据根据结果进行相应的处理
            $("#viewcount").html(result.browseCount + "&nbsp;人浏览过");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

//更新喜欢数量
function updateLikeCount(el) {
    // var el = $(this);
    el.addClass('active');
    $.ajax({
        type : "POST",  //提交方式
        url : suffix +"/updateLikeCount",//路径
        data : {
            "shareId":shareId
        },//数据，这里使用的是Json格式进行传输
        dataType:'json',
        success : function(result) {//返回数据根据结果进行相应的处理
            $("#likecount").text(result.likeCount);
            setTimeout(function() {
                el.removeClass('active').addClass('active1');
            }, 600);
        }
    });
}

/**
 *
 * @returns {*}
 */
function getFilePath() {
    return fileName;
}

/**
 * 加载全景播放器组件
 * @param type 组件类型 图片、视频
 * @param filePath
 */
function loadPano(type) {
    if (type == "video") {
        // 如果是android手机，由于不兼容，仅显示视频截图
        if (isAndroid){
            var filePath = getFilePath();
            embedpano({
                swf: "plugins/krpano.swf",
                xml: "xml/photo.xml",
                target: "pano",
                html5: "auto",
                initvars:{videoPath:filePath}, // key:value
                passQueryParameters:false
            });
        }
        // 如果是ios手机，或则PC...，则显示视频
        else {
            document.body.className="video";
            // PS：此处通过js函数传递参数
            embedpano({
                swf: "plugins/krpano.swf",
                xml: "xml/video.xml",
                target: "pano",
                html5: "auto",
                initvars:{videoPath:filePath},
                passQueryParameters:false

            });
        }
    }
    else if (type == "photo") {
        // TODO：图片找不到的异常提示，可以参考视频的做法，目前是没有的
        var filePath = getFilePath();
        embedpano({
            swf: "plugins/krpano.swf",
            xml: "xml/photo.xml?a=0",
            target: "pano",
            html5: "auto",
            initvars:{photoPath:filePath},

            //
            // initvars:{
            //     photoPath:filePath,
            //     dishDistortion: 1, //默认渐变后畸变大小 默认1
            //     enableAutorotate: false,
            //     // autorotateSpeed: 20,
            //     fisheyeFov: 90,
            //     viewController: 'drag', //控制方式 'gyro'|'drag'
            //     debug: false,
            //     design:"custom",
            //     maxpixelzoom: "0.8",
            //     locale: "zh",
            //     //以下为相册配置
            // },
            passQueryParameters:false
        });
    }
}
